(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 112,
	height: 65,
	fps: 30,
	color: "#FFFFFF",
	manifest: [
		{src:"images/logo1.png", id:"logo1"},
		{src:"images/logo2.png", id:"logo2"}
	]
};

// stage content:
(lib.logo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_270 = function() {
		this.gotoAndPlay(1)
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(270).call(this.frame_270).wait(1));

	// mascara (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AiPBYICTkAICMBRIiTEAg");
	var mask_graphics_7 = new cjs.Graphics().p("AgaCUIgHALIiMhRICVkAIAzAfIAHgLICMBRIiVEAg");
	var mask_graphics_13 = new cjs.Graphics().p("Ag+CPIgGALIiMhRICVj/IA1AeIAGgLIDRBlIiwD1g");
	var mask_graphics_19 = new cjs.Graphics().p("AhRCKIgHALIiMhRICWkAIA1AfIAGgLID3BtIiND4g");
	var mask_graphics_25 = new cjs.Graphics().p("AhlCJIgHALIiMhRICVj/IA1AeIAHgLIEgBvIiID4g");
	var mask_graphics_31 = new cjs.Graphics().p("AhzCGIgHALIiMhRICVj/IA1AeIAHgLIE8BvIh2D9g");
	var mask_graphics_37 = new cjs.Graphics().p("AiXCFIgGAMIiMhRICVkAIA1AeIAGgLIGDByIh+D8g");
	var mask_graphics_43 = new cjs.Graphics().p("Ai7CFIgHAMIiMhRICWkAIA1AeIAGgLIHMBxIh8D9g");
	var mask_graphics_49 = new cjs.Graphics().p("AjgCFIgGALIiMhRICVkAIA1AfIAGgMIIVB1IiBD7g");
	var mask_graphics_55 = new cjs.Graphics().p("AkJCDIgGAMIiMhRICVkAIA1AeIAGgLIJnB3IiUD7g");
	var mask_graphics_61 = new cjs.Graphics().p("AkaCHIgGALIiMhRICVj/IA1AeIAGgLIKJB1IAAD1g");
	var mask_graphics_270 = new cjs.Graphics().p("AkaCHIgGALIiMhRICVj/IA1AeIAGgLIKJB1IAAD1g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:30.2,y:24.7}).wait(7).to({graphics:mask_graphics_7,x:33.1,y:25.6}).wait(6).to({graphics:mask_graphics_13,x:36.7,y:26.1}).wait(6).to({graphics:mask_graphics_19,x:38.6,y:26.6}).wait(6).to({graphics:mask_graphics_25,x:40.6,y:26.7}).wait(6).to({graphics:mask_graphics_31,x:42,y:27}).wait(6).to({graphics:mask_graphics_37,x:45.6,y:27.1}).wait(6).to({graphics:mask_graphics_43,x:49.2,y:27.1}).wait(6).to({graphics:mask_graphics_49,x:52.9,y:27.2}).wait(6).to({graphics:mask_graphics_55,x:57,y:27.3}).wait(6).to({graphics:mask_graphics_61,x:58.7,y:26.9}).wait(209).to({graphics:mask_graphics_270,x:58.7,y:26.9}).wait(1));

	// Capa 2
	this.instance = new lib.Símbolo2();
	this.instance.setTransform(56,32.5,1,1,0,0,0,53.5,30);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(271));

	// Capa 1
	this.instance_1 = new lib.Símbolo1();
	this.instance_1.setTransform(56,32.5,1,1,0,0,0,53.5,30);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(271));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(58.5,35,107,60);


// symbols:
(lib.logo1 = function() {
	this.initialize(img.logo1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,107,60);


(lib.logo2 = function() {
	this.initialize(img.logo2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,107,60);


(lib.Símbolo2 = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.logo2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,107,60);


(lib.Símbolo1 = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.logo1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,107,60);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;