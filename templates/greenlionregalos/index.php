<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Joomla Head -->
        <jdoc:include type="head" />

        <!-- Fonts-->
        
        <!-- Less -->
        <link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/reset.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="less/load-styles.php?load=regalos"/>

        <!-- Script -->

        <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/jquery-ui.js"></script>
        <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/smoothness/jquery-ui-1.10.3.custom.css" rel="stylesheet">
        
    </head>

    <body>

        <!--Primera section-->
        
        <div class="section page-one" id="section-one">
            <jdoc:include type="modules" name="background" style="xhtml" /> 
            <header>
                <div class="content">
                    <div class="content-up">
                        <div class="logo">
                            <iframe src="html5/logo.html" width="122" height="69" style="margin-top: 4px;"></iframe>
                        </div>
                        <nav>
                            <jdoc:include type="modules" name="menu" style="xhtml" /> 
                        </nav>
                        <div class="carrito">
                            <jdoc:include type="modules" name="carro" style="xhtml" /> 
                        </div>
                        <div class="login">
                            <jdoc:include type="modules" name="login" style="xhtml" /> 
                        </div>
                    </div>
                </div>
            </header>
            <div class="component">
                <jdoc:include type="message" />
            </div>
            <main>
                <div class="wrapper-main-regalos">
                    <jdoc:include type="modules" name="menu-banner" style="xhtml" /> 
                    <jdoc:include type="component" /> 
                    <jdoc:include type="modules" name="vino" style="xhtml" /> 
                </div>
            </main>

        <!--PIE DE LA PAGINA-->

        <div class="section page-seven">
            <div class="content">
                <jdoc:include type="modules" name="footer" style="xhtml" /> 
                <div class="down">
                    <a href="#" class="go-to-header"><img src="images/page-one/flechaArriba.png"/></a>
                </div>
                <div class="copy">
                    <span class="sainet">
                        <a target="_blank" href="http://www.creandopaginasweb.com">
                            Página web diseñada por <img alt="Diseño de paginas web" src="http://www.creandopaginasweb.com/theme/img/logo_blanco.png">
                        </a>
                    </span>
                </div>
            </div>
        </div>
    </body>
    <script src="js/acciones.js"></script>
</html>
<?php 
if(  $_GET["acceso"]=="mal" && $_SESSION['__default']['user']->id==0){
    ?>
    <script type="text/javascript">
    alert("Usuario o contraseña incorrectos.");
    </script>
    <?php 
}
?>