//     Underscore.js 1.6.0
//     http://underscorejs.org
//     (c) 2009-2014 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.
(function(){var n=this,t=n._,r={},e=Array.prototype,u=Object.prototype,i=Function.prototype,a=e.push,o=e.slice,c=e.concat,l=u.toString,f=u.hasOwnProperty,s=e.forEach,p=e.map,h=e.reduce,v=e.reduceRight,g=e.filter,d=e.every,m=e.some,y=e.indexOf,b=e.lastIndexOf,x=Array.isArray,w=Object.keys,_=i.bind,j=function(n){return n instanceof j?n:this instanceof j?void(this._wrapped=n):new j(n)};"undefined"!=typeof exports?("undefined"!=typeof module&&module.exports&&(exports=module.exports=j),exports._=j):n._=j,j.VERSION="1.6.0";var A=j.each=j.forEach=function(n,t,e){if(null==n)return n;if(s&&n.forEach===s)n.forEach(t,e);else if(n.length===+n.length){for(var u=0,i=n.length;i>u;u++)if(t.call(e,n[u],u,n)===r)return}else for(var a=j.keys(n),u=0,i=a.length;i>u;u++)if(t.call(e,n[a[u]],a[u],n)===r)return;return n};j.map=j.collect=function(n,t,r){var e=[];return null==n?e:p&&n.map===p?n.map(t,r):(A(n,function(n,u,i){e.push(t.call(r,n,u,i))}),e)};var O="Reduce of empty array with no initial value";j.reduce=j.foldl=j.inject=function(n,t,r,e){var u=arguments.length>2;if(null==n&&(n=[]),h&&n.reduce===h)return e&&(t=j.bind(t,e)),u?n.reduce(t,r):n.reduce(t);if(A(n,function(n,i,a){u?r=t.call(e,r,n,i,a):(r=n,u=!0)}),!u)throw new TypeError(O);return r},j.reduceRight=j.foldr=function(n,t,r,e){var u=arguments.length>2;if(null==n&&(n=[]),v&&n.reduceRight===v)return e&&(t=j.bind(t,e)),u?n.reduceRight(t,r):n.reduceRight(t);var i=n.length;if(i!==+i){var a=j.keys(n);i=a.length}if(A(n,function(o,c,l){c=a?a[--i]:--i,u?r=t.call(e,r,n[c],c,l):(r=n[c],u=!0)}),!u)throw new TypeError(O);return r},j.find=j.detect=function(n,t,r){var e;return k(n,function(n,u,i){return t.call(r,n,u,i)?(e=n,!0):void 0}),e},j.filter=j.select=function(n,t,r){var e=[];return null==n?e:g&&n.filter===g?n.filter(t,r):(A(n,function(n,u,i){t.call(r,n,u,i)&&e.push(n)}),e)},j.reject=function(n,t,r){return j.filter(n,function(n,e,u){return!t.call(r,n,e,u)},r)},j.every=j.all=function(n,t,e){t||(t=j.identity);var u=!0;return null==n?u:d&&n.every===d?n.every(t,e):(A(n,function(n,i,a){return(u=u&&t.call(e,n,i,a))?void 0:r}),!!u)};var k=j.some=j.any=function(n,t,e){t||(t=j.identity);var u=!1;return null==n?u:m&&n.some===m?n.some(t,e):(A(n,function(n,i,a){return u||(u=t.call(e,n,i,a))?r:void 0}),!!u)};j.contains=j.include=function(n,t){return null==n?!1:y&&n.indexOf===y?n.indexOf(t)!=-1:k(n,function(n){return n===t})},j.invoke=function(n,t){var r=o.call(arguments,2),e=j.isFunction(t);return j.map(n,function(n){return(e?t:n[t]).apply(n,r)})},j.pluck=function(n,t){return j.map(n,j.property(t))},j.where=function(n,t){return j.filter(n,j.matches(t))},j.findWhere=function(n,t){return j.find(n,j.matches(t))},j.max=function(n,t,r){if(!t&&j.isArray(n)&&n[0]===+n[0]&&n.length<65535)return Math.max.apply(Math,n);var e=-1/0,u=-1/0;return A(n,function(n,i,a){var o=t?t.call(r,n,i,a):n;o>u&&(e=n,u=o)}),e},j.min=function(n,t,r){if(!t&&j.isArray(n)&&n[0]===+n[0]&&n.length<65535)return Math.min.apply(Math,n);var e=1/0,u=1/0;return A(n,function(n,i,a){var o=t?t.call(r,n,i,a):n;u>o&&(e=n,u=o)}),e},j.shuffle=function(n){var t,r=0,e=[];return A(n,function(n){t=j.random(r++),e[r-1]=e[t],e[t]=n}),e},j.sample=function(n,t,r){return null==t||r?(n.length!==+n.length&&(n=j.values(n)),n[j.random(n.length-1)]):j.shuffle(n).slice(0,Math.max(0,t))};var E=function(n){return null==n?j.identity:j.isFunction(n)?n:j.property(n)};j.sortBy=function(n,t,r){return t=E(t),j.pluck(j.map(n,function(n,e,u){return{value:n,index:e,criteria:t.call(r,n,e,u)}}).sort(function(n,t){var r=n.criteria,e=t.criteria;if(r!==e){if(r>e||r===void 0)return 1;if(e>r||e===void 0)return-1}return n.index-t.index}),"value")};var F=function(n){return function(t,r,e){var u={};return r=E(r),A(t,function(i,a){var o=r.call(e,i,a,t);n(u,o,i)}),u}};j.groupBy=F(function(n,t,r){j.has(n,t)?n[t].push(r):n[t]=[r]}),j.indexBy=F(function(n,t,r){n[t]=r}),j.countBy=F(function(n,t){j.has(n,t)?n[t]++:n[t]=1}),j.sortedIndex=function(n,t,r,e){r=E(r);for(var u=r.call(e,t),i=0,a=n.length;a>i;){var o=i+a>>>1;r.call(e,n[o])<u?i=o+1:a=o}return i},j.toArray=function(n){return n?j.isArray(n)?o.call(n):n.length===+n.length?j.map(n,j.identity):j.values(n):[]},j.size=function(n){return null==n?0:n.length===+n.length?n.length:j.keys(n).length},j.first=j.head=j.take=function(n,t,r){return null==n?void 0:null==t||r?n[0]:0>t?[]:o.call(n,0,t)},j.initial=function(n,t,r){return o.call(n,0,n.length-(null==t||r?1:t))},j.last=function(n,t,r){return null==n?void 0:null==t||r?n[n.length-1]:o.call(n,Math.max(n.length-t,0))},j.rest=j.tail=j.drop=function(n,t,r){return o.call(n,null==t||r?1:t)},j.compact=function(n){return j.filter(n,j.identity)};var M=function(n,t,r){return t&&j.every(n,j.isArray)?c.apply(r,n):(A(n,function(n){j.isArray(n)||j.isArguments(n)?t?a.apply(r,n):M(n,t,r):r.push(n)}),r)};j.flatten=function(n,t){return M(n,t,[])},j.without=function(n){return j.difference(n,o.call(arguments,1))},j.partition=function(n,t){var r=[],e=[];return A(n,function(n){(t(n)?r:e).push(n)}),[r,e]},j.uniq=j.unique=function(n,t,r,e){j.isFunction(t)&&(e=r,r=t,t=!1);var u=r?j.map(n,r,e):n,i=[],a=[];return A(u,function(r,e){(t?e&&a[a.length-1]===r:j.contains(a,r))||(a.push(r),i.push(n[e]))}),i},j.union=function(){return j.uniq(j.flatten(arguments,!0))},j.intersection=function(n){var t=o.call(arguments,1);return j.filter(j.uniq(n),function(n){return j.every(t,function(t){return j.contains(t,n)})})},j.difference=function(n){var t=c.apply(e,o.call(arguments,1));return j.filter(n,function(n){return!j.contains(t,n)})},j.zip=function(){for(var n=j.max(j.pluck(arguments,"length").concat(0)),t=new Array(n),r=0;n>r;r++)t[r]=j.pluck(arguments,""+r);return t},j.object=function(n,t){if(null==n)return{};for(var r={},e=0,u=n.length;u>e;e++)t?r[n[e]]=t[e]:r[n[e][0]]=n[e][1];return r},j.indexOf=function(n,t,r){if(null==n)return-1;var e=0,u=n.length;if(r){if("number"!=typeof r)return e=j.sortedIndex(n,t),n[e]===t?e:-1;e=0>r?Math.max(0,u+r):r}if(y&&n.indexOf===y)return n.indexOf(t,r);for(;u>e;e++)if(n[e]===t)return e;return-1},j.lastIndexOf=function(n,t,r){if(null==n)return-1;var e=null!=r;if(b&&n.lastIndexOf===b)return e?n.lastIndexOf(t,r):n.lastIndexOf(t);for(var u=e?r:n.length;u--;)if(n[u]===t)return u;return-1},j.range=function(n,t,r){arguments.length<=1&&(t=n||0,n=0),r=arguments[2]||1;for(var e=Math.max(Math.ceil((t-n)/r),0),u=0,i=new Array(e);e>u;)i[u++]=n,n+=r;return i};var R=function(){};j.bind=function(n,t){var r,e;if(_&&n.bind===_)return _.apply(n,o.call(arguments,1));if(!j.isFunction(n))throw new TypeError;return r=o.call(arguments,2),e=function(){if(!(this instanceof e))return n.apply(t,r.concat(o.call(arguments)));R.prototype=n.prototype;var u=new R;R.prototype=null;var i=n.apply(u,r.concat(o.call(arguments)));return Object(i)===i?i:u}},j.partial=function(n){var t=o.call(arguments,1);return function(){for(var r=0,e=t.slice(),u=0,i=e.length;i>u;u++)e[u]===j&&(e[u]=arguments[r++]);for(;r<arguments.length;)e.push(arguments[r++]);return n.apply(this,e)}},j.bindAll=function(n){var t=o.call(arguments,1);if(0===t.length)throw new Error("bindAll must be passed function names");return A(t,function(t){n[t]=j.bind(n[t],n)}),n},j.memoize=function(n,t){var r={};return t||(t=j.identity),function(){var e=t.apply(this,arguments);return j.has(r,e)?r[e]:r[e]=n.apply(this,arguments)}},j.delay=function(n,t){var r=o.call(arguments,2);return setTimeout(function(){return n.apply(null,r)},t)},j.defer=function(n){return j.delay.apply(j,[n,1].concat(o.call(arguments,1)))},j.throttle=function(n,t,r){var e,u,i,a=null,o=0;r||(r={});var c=function(){o=r.leading===!1?0:j.now(),a=null,i=n.apply(e,u),e=u=null};return function(){var l=j.now();o||r.leading!==!1||(o=l);var f=t-(l-o);return e=this,u=arguments,0>=f?(clearTimeout(a),a=null,o=l,i=n.apply(e,u),e=u=null):a||r.trailing===!1||(a=setTimeout(c,f)),i}},j.debounce=function(n,t,r){var e,u,i,a,o,c=function(){var l=j.now()-a;t>l?e=setTimeout(c,t-l):(e=null,r||(o=n.apply(i,u),i=u=null))};return function(){i=this,u=arguments,a=j.now();var l=r&&!e;return e||(e=setTimeout(c,t)),l&&(o=n.apply(i,u),i=u=null),o}},j.once=function(n){var t,r=!1;return function(){return r?t:(r=!0,t=n.apply(this,arguments),n=null,t)}},j.wrap=function(n,t){return j.partial(t,n)},j.compose=function(){var n=arguments;return function(){for(var t=arguments,r=n.length-1;r>=0;r--)t=[n[r].apply(this,t)];return t[0]}},j.after=function(n,t){return function(){return--n<1?t.apply(this,arguments):void 0}},j.keys=function(n){if(!j.isObject(n))return[];if(w)return w(n);var t=[];for(var r in n)j.has(n,r)&&t.push(r);return t},j.values=function(n){for(var t=j.keys(n),r=t.length,e=new Array(r),u=0;r>u;u++)e[u]=n[t[u]];return e},j.pairs=function(n){for(var t=j.keys(n),r=t.length,e=new Array(r),u=0;r>u;u++)e[u]=[t[u],n[t[u]]];return e},j.invert=function(n){for(var t={},r=j.keys(n),e=0,u=r.length;u>e;e++)t[n[r[e]]]=r[e];return t},j.functions=j.methods=function(n){var t=[];for(var r in n)j.isFunction(n[r])&&t.push(r);return t.sort()},j.extend=function(n){return A(o.call(arguments,1),function(t){if(t)for(var r in t)n[r]=t[r]}),n},j.pick=function(n){var t={},r=c.apply(e,o.call(arguments,1));return A(r,function(r){r in n&&(t[r]=n[r])}),t},j.omit=function(n){var t={},r=c.apply(e,o.call(arguments,1));for(var u in n)j.contains(r,u)||(t[u]=n[u]);return t},j.defaults=function(n){return A(o.call(arguments,1),function(t){if(t)for(var r in t)n[r]===void 0&&(n[r]=t[r])}),n},j.clone=function(n){return j.isObject(n)?j.isArray(n)?n.slice():j.extend({},n):n},j.tap=function(n,t){return t(n),n};var S=function(n,t,r,e){if(n===t)return 0!==n||1/n==1/t;if(null==n||null==t)return n===t;n instanceof j&&(n=n._wrapped),t instanceof j&&(t=t._wrapped);var u=l.call(n);if(u!=l.call(t))return!1;switch(u){case"[object String]":return n==String(t);case"[object Number]":return n!=+n?t!=+t:0==n?1/n==1/t:n==+t;case"[object Date]":case"[object Boolean]":return+n==+t;case"[object RegExp]":return n.source==t.source&&n.global==t.global&&n.multiline==t.multiline&&n.ignoreCase==t.ignoreCase}if("object"!=typeof n||"object"!=typeof t)return!1;for(var i=r.length;i--;)if(r[i]==n)return e[i]==t;var a=n.constructor,o=t.constructor;if(a!==o&&!(j.isFunction(a)&&a instanceof a&&j.isFunction(o)&&o instanceof o)&&"constructor"in n&&"constructor"in t)return!1;r.push(n),e.push(t);var c=0,f=!0;if("[object Array]"==u){if(c=n.length,f=c==t.length)for(;c--&&(f=S(n[c],t[c],r,e)););}else{for(var s in n)if(j.has(n,s)&&(c++,!(f=j.has(t,s)&&S(n[s],t[s],r,e))))break;if(f){for(s in t)if(j.has(t,s)&&!c--)break;f=!c}}return r.pop(),e.pop(),f};j.isEqual=function(n,t){return S(n,t,[],[])},j.isEmpty=function(n){if(null==n)return!0;if(j.isArray(n)||j.isString(n))return 0===n.length;for(var t in n)if(j.has(n,t))return!1;return!0},j.isElement=function(n){return!(!n||1!==n.nodeType)},j.isArray=x||function(n){return"[object Array]"==l.call(n)},j.isObject=function(n){return n===Object(n)},A(["Arguments","Function","String","Number","Date","RegExp"],function(n){j["is"+n]=function(t){return l.call(t)=="[object "+n+"]"}}),j.isArguments(arguments)||(j.isArguments=function(n){return!(!n||!j.has(n,"callee"))}),"function"!=typeof/./&&(j.isFunction=function(n){return"function"==typeof n}),j.isFinite=function(n){return isFinite(n)&&!isNaN(parseFloat(n))},j.isNaN=function(n){return j.isNumber(n)&&n!=+n},j.isBoolean=function(n){return n===!0||n===!1||"[object Boolean]"==l.call(n)},j.isNull=function(n){return null===n},j.isUndefined=function(n){return n===void 0},j.has=function(n,t){return f.call(n,t)},j.noConflict=function(){return n._=t,this},j.identity=function(n){return n},j.constant=function(n){return function(){return n}},j.property=function(n){return function(t){return t[n]}},j.matches=function(n){return function(t){if(t===n)return!0;for(var r in n)if(n[r]!==t[r])return!1;return!0}},j.times=function(n,t,r){for(var e=Array(Math.max(0,n)),u=0;n>u;u++)e[u]=t.call(r,u);return e},j.random=function(n,t){return null==t&&(t=n,n=0),n+Math.floor(Math.random()*(t-n+1))},j.now=Date.now||function(){return(new Date).getTime()};var T={escape:{"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;"}};T.unescape=j.invert(T.escape);var I={escape:new RegExp("["+j.keys(T.escape).join("")+"]","g"),unescape:new RegExp("("+j.keys(T.unescape).join("|")+")","g")};j.each(["escape","unescape"],function(n){j[n]=function(t){return null==t?"":(""+t).replace(I[n],function(t){return T[n][t]})}}),j.result=function(n,t){if(null==n)return void 0;var r=n[t];return j.isFunction(r)?r.call(n):r},j.mixin=function(n){A(j.functions(n),function(t){var r=j[t]=n[t];j.prototype[t]=function(){var n=[this._wrapped];return a.apply(n,arguments),z.call(this,r.apply(j,n))}})};var N=0;j.uniqueId=function(n){var t=++N+"";return n?n+t:t},j.templateSettings={evaluate:/<%([\s\S]+?)%>/g,interpolate:/<%=([\s\S]+?)%>/g,escape:/<%-([\s\S]+?)%>/g};var q=/(.)^/,B={"'":"'","\\":"\\","\r":"r","\n":"n","	":"t","\u2028":"u2028","\u2029":"u2029"},D=/\\|'|\r|\n|\t|\u2028|\u2029/g;j.template=function(n,t,r){var e;r=j.defaults({},r,j.templateSettings);var u=new RegExp([(r.escape||q).source,(r.interpolate||q).source,(r.evaluate||q).source].join("|")+"|$","g"),i=0,a="__p+='";n.replace(u,function(t,r,e,u,o){return a+=n.slice(i,o).replace(D,function(n){return"\\"+B[n]}),r&&(a+="'+\n((__t=("+r+"))==null?'':_.escape(__t))+\n'"),e&&(a+="'+\n((__t=("+e+"))==null?'':__t)+\n'"),u&&(a+="';\n"+u+"\n__p+='"),i=o+t.length,t}),a+="';\n",r.variable||(a="with(obj||{}){\n"+a+"}\n"),a="var __t,__p='',__j=Array.prototype.join,"+"print=function(){__p+=__j.call(arguments,'');};\n"+a+"return __p;\n";try{e=new Function(r.variable||"obj","_",a)}catch(o){throw o.source=a,o}if(t)return e(t,j);var c=function(n){return e.call(this,n,j)};return c.source="function("+(r.variable||"obj")+"){\n"+a+"}",c},j.chain=function(n){return j(n).chain()};var z=function(n){return this._chain?j(n).chain():n};j.mixin(j),A(["pop","push","reverse","shift","sort","splice","unshift"],function(n){var t=e[n];j.prototype[n]=function(){var r=this._wrapped;return t.apply(r,arguments),"shift"!=n&&"splice"!=n||0!==r.length||delete r[0],z.call(this,r)}}),A(["concat","join","slice"],function(n){var t=e[n];j.prototype[n]=function(){return z.call(this,t.apply(this._wrapped,arguments))}}),j.extend(j.prototype,{chain:function(){return this._chain=!0,this},value:function(){return this._wrapped}}),"function"==typeof define&&define.amd&&define("underscore",[],function(){return j})}).call(this);
//# sourceMappingURL=underscore-min.map
(function(t,e){if(typeof define==="function"&&define.amd){define(["underscore","jquery","exports"],function(i,r,s){t.Backbone=e(t,s,i,r)})}else if(typeof exports!=="undefined"){var i=require("underscore");e(t,exports,i)}else{t.Backbone=e(t,{},t._,t.jQuery||t.Zepto||t.ender||t.$)}})(this,function(t,e,i,r){var s=t.Backbone;var n=[];var a=n.push;var o=n.slice;var h=n.splice;e.VERSION="1.1.2";e.$=r;e.noConflict=function(){t.Backbone=s;return this};e.emulateHTTP=false;e.emulateJSON=false;var u=e.Events={on:function(t,e,i){if(!c(this,"on",t,[e,i])||!e)return this;this._events||(this._events={});var r=this._events[t]||(this._events[t]=[]);r.push({callback:e,context:i,ctx:i||this});return this},once:function(t,e,r){if(!c(this,"once",t,[e,r])||!e)return this;var s=this;var n=i.once(function(){s.off(t,n);e.apply(this,arguments)});n._callback=e;return this.on(t,n,r)},off:function(t,e,r){var s,n,a,o,h,u,l,f;if(!this._events||!c(this,"off",t,[e,r]))return this;if(!t&&!e&&!r){this._events=void 0;return this}o=t?[t]:i.keys(this._events);for(h=0,u=o.length;h<u;h++){t=o[h];if(a=this._events[t]){this._events[t]=s=[];if(e||r){for(l=0,f=a.length;l<f;l++){n=a[l];if(e&&e!==n.callback&&e!==n.callback._callback||r&&r!==n.context){s.push(n)}}}if(!s.length)delete this._events[t]}}return this},trigger:function(t){if(!this._events)return this;var e=o.call(arguments,1);if(!c(this,"trigger",t,e))return this;var i=this._events[t];var r=this._events.all;if(i)f(i,e);if(r)f(r,arguments);return this},stopListening:function(t,e,r){var s=this._listeningTo;if(!s)return this;var n=!e&&!r;if(!r&&typeof e==="object")r=this;if(t)(s={})[t._listenId]=t;for(var a in s){t=s[a];t.off(e,r,this);if(n||i.isEmpty(t._events))delete this._listeningTo[a]}return this}};var l=/\s+/;var c=function(t,e,i,r){if(!i)return true;if(typeof i==="object"){for(var s in i){t[e].apply(t,[s,i[s]].concat(r))}return false}if(l.test(i)){var n=i.split(l);for(var a=0,o=n.length;a<o;a++){t[e].apply(t,[n[a]].concat(r))}return false}return true};var f=function(t,e){var i,r=-1,s=t.length,n=e[0],a=e[1],o=e[2];switch(e.length){case 0:while(++r<s)(i=t[r]).callback.call(i.ctx);return;case 1:while(++r<s)(i=t[r]).callback.call(i.ctx,n);return;case 2:while(++r<s)(i=t[r]).callback.call(i.ctx,n,a);return;case 3:while(++r<s)(i=t[r]).callback.call(i.ctx,n,a,o);return;default:while(++r<s)(i=t[r]).callback.apply(i.ctx,e);return}};var d={listenTo:"on",listenToOnce:"once"};i.each(d,function(t,e){u[e]=function(e,r,s){var n=this._listeningTo||(this._listeningTo={});var a=e._listenId||(e._listenId=i.uniqueId("l"));n[a]=e;if(!s&&typeof r==="object")s=this;e[t](r,s,this);return this}});u.bind=u.on;u.unbind=u.off;i.extend(e,u);var p=e.Model=function(t,e){var r=t||{};e||(e={});this.cid=i.uniqueId("c");this.attributes={};if(e.collection)this.collection=e.collection;if(e.parse)r=this.parse(r,e)||{};r=i.defaults({},r,i.result(this,"defaults"));this.set(r,e);this.changed={};this.initialize.apply(this,arguments)};i.extend(p.prototype,u,{changed:null,validationError:null,idAttribute:"id",initialize:function(){},toJSON:function(t){return i.clone(this.attributes)},sync:function(){return e.sync.apply(this,arguments)},get:function(t){return this.attributes[t]},escape:function(t){return i.escape(this.get(t))},has:function(t){return this.get(t)!=null},set:function(t,e,r){var s,n,a,o,h,u,l,c;if(t==null)return this;if(typeof t==="object"){n=t;r=e}else{(n={})[t]=e}r||(r={});if(!this._validate(n,r))return false;a=r.unset;h=r.silent;o=[];u=this._changing;this._changing=true;if(!u){this._previousAttributes=i.clone(this.attributes);this.changed={}}c=this.attributes,l=this._previousAttributes;if(this.idAttribute in n)this.id=n[this.idAttribute];for(s in n){e=n[s];if(!i.isEqual(c[s],e))o.push(s);if(!i.isEqual(l[s],e)){this.changed[s]=e}else{delete this.changed[s]}a?delete c[s]:c[s]=e}if(!h){if(o.length)this._pending=r;for(var f=0,d=o.length;f<d;f++){this.trigger("change:"+o[f],this,c[o[f]],r)}}if(u)return this;if(!h){while(this._pending){r=this._pending;this._pending=false;this.trigger("change",this,r)}}this._pending=false;this._changing=false;return this},unset:function(t,e){return this.set(t,void 0,i.extend({},e,{unset:true}))},clear:function(t){var e={};for(var r in this.attributes)e[r]=void 0;return this.set(e,i.extend({},t,{unset:true}))},hasChanged:function(t){if(t==null)return!i.isEmpty(this.changed);return i.has(this.changed,t)},changedAttributes:function(t){if(!t)return this.hasChanged()?i.clone(this.changed):false;var e,r=false;var s=this._changing?this._previousAttributes:this.attributes;for(var n in t){if(i.isEqual(s[n],e=t[n]))continue;(r||(r={}))[n]=e}return r},previous:function(t){if(t==null||!this._previousAttributes)return null;return this._previousAttributes[t]},previousAttributes:function(){return i.clone(this._previousAttributes)},fetch:function(t){t=t?i.clone(t):{};if(t.parse===void 0)t.parse=true;var e=this;var r=t.success;t.success=function(i){if(!e.set(e.parse(i,t),t))return false;if(r)r(e,i,t);e.trigger("sync",e,i,t)};q(this,t);return this.sync("read",this,t)},save:function(t,e,r){var s,n,a,o=this.attributes;if(t==null||typeof t==="object"){s=t;r=e}else{(s={})[t]=e}r=i.extend({validate:true},r);if(s&&!r.wait){if(!this.set(s,r))return false}else{if(!this._validate(s,r))return false}if(s&&r.wait){this.attributes=i.extend({},o,s)}if(r.parse===void 0)r.parse=true;var h=this;var u=r.success;r.success=function(t){h.attributes=o;var e=h.parse(t,r);if(r.wait)e=i.extend(s||{},e);if(i.isObject(e)&&!h.set(e,r)){return false}if(u)u(h,t,r);h.trigger("sync",h,t,r)};q(this,r);n=this.isNew()?"create":r.patch?"patch":"update";if(n==="patch")r.attrs=s;a=this.sync(n,this,r);if(s&&r.wait)this.attributes=o;return a},destroy:function(t){t=t?i.clone(t):{};var e=this;var r=t.success;var s=function(){e.trigger("destroy",e,e.collection,t)};t.success=function(i){if(t.wait||e.isNew())s();if(r)r(e,i,t);if(!e.isNew())e.trigger("sync",e,i,t)};if(this.isNew()){t.success();return false}q(this,t);var n=this.sync("delete",this,t);if(!t.wait)s();return n},url:function(){var t=i.result(this,"urlRoot")||i.result(this.collection,"url")||M();if(this.isNew())return t;return t.replace(/([^\/])$/,"$1/")+encodeURIComponent(this.id)},parse:function(t,e){return t},clone:function(){return new this.constructor(this.attributes)},isNew:function(){return!this.has(this.idAttribute)},isValid:function(t){return this._validate({},i.extend(t||{},{validate:true}))},_validate:function(t,e){if(!e.validate||!this.validate)return true;t=i.extend({},this.attributes,t);var r=this.validationError=this.validate(t,e)||null;if(!r)return true;this.trigger("invalid",this,r,i.extend(e,{validationError:r}));return false}});var v=["keys","values","pairs","invert","pick","omit"];i.each(v,function(t){p.prototype[t]=function(){var e=o.call(arguments);e.unshift(this.attributes);return i[t].apply(i,e)}});var g=e.Collection=function(t,e){e||(e={});if(e.model)this.model=e.model;if(e.comparator!==void 0)this.comparator=e.comparator;this._reset();this.initialize.apply(this,arguments);if(t)this.reset(t,i.extend({silent:true},e))};var m={add:true,remove:true,merge:true};var y={add:true,remove:false};i.extend(g.prototype,u,{model:p,initialize:function(){},toJSON:function(t){return this.map(function(e){return e.toJSON(t)})},sync:function(){return e.sync.apply(this,arguments)},add:function(t,e){return this.set(t,i.extend({merge:false},e,y))},remove:function(t,e){var r=!i.isArray(t);t=r?[t]:i.clone(t);e||(e={});var s,n,a,o;for(s=0,n=t.length;s<n;s++){o=t[s]=this.get(t[s]);if(!o)continue;delete this._byId[o.id];delete this._byId[o.cid];a=this.indexOf(o);this.models.splice(a,1);this.length--;if(!e.silent){e.index=a;o.trigger("remove",o,this,e)}this._removeReference(o,e)}return r?t[0]:t},set:function(t,e){e=i.defaults({},e,m);if(e.parse)t=this.parse(t,e);var r=!i.isArray(t);t=r?t?[t]:[]:i.clone(t);var s,n,a,o,h,u,l;var c=e.at;var f=this.model;var d=this.comparator&&c==null&&e.sort!==false;var v=i.isString(this.comparator)?this.comparator:null;var g=[],y=[],_={};var b=e.add,w=e.merge,x=e.remove;var E=!d&&b&&x?[]:false;for(s=0,n=t.length;s<n;s++){h=t[s]||{};if(h instanceof p){a=o=h}else{a=h[f.prototype.idAttribute||"id"]}if(u=this.get(a)){if(x)_[u.cid]=true;if(w){h=h===o?o.attributes:h;if(e.parse)h=u.parse(h,e);u.set(h,e);if(d&&!l&&u.hasChanged(v))l=true}t[s]=u}else if(b){o=t[s]=this._prepareModel(h,e);if(!o)continue;g.push(o);this._addReference(o,e)}o=u||o;if(E&&(o.isNew()||!_[o.id]))E.push(o);_[o.id]=true}if(x){for(s=0,n=this.length;s<n;++s){if(!_[(o=this.models[s]).cid])y.push(o)}if(y.length)this.remove(y,e)}if(g.length||E&&E.length){if(d)l=true;this.length+=g.length;if(c!=null){for(s=0,n=g.length;s<n;s++){this.models.splice(c+s,0,g[s])}}else{if(E)this.models.length=0;var k=E||g;for(s=0,n=k.length;s<n;s++){this.models.push(k[s])}}}if(l)this.sort({silent:true});if(!e.silent){for(s=0,n=g.length;s<n;s++){(o=g[s]).trigger("add",o,this,e)}if(l||E&&E.length)this.trigger("sort",this,e)}return r?t[0]:t},reset:function(t,e){e||(e={});for(var r=0,s=this.models.length;r<s;r++){this._removeReference(this.models[r],e)}e.previousModels=this.models;this._reset();t=this.add(t,i.extend({silent:true},e));if(!e.silent)this.trigger("reset",this,e);return t},push:function(t,e){return this.add(t,i.extend({at:this.length},e))},pop:function(t){var e=this.at(this.length-1);this.remove(e,t);return e},unshift:function(t,e){return this.add(t,i.extend({at:0},e))},shift:function(t){var e=this.at(0);this.remove(e,t);return e},slice:function(){return o.apply(this.models,arguments)},get:function(t){if(t==null)return void 0;return this._byId[t]||this._byId[t.id]||this._byId[t.cid]},at:function(t){return this.models[t]},where:function(t,e){if(i.isEmpty(t))return e?void 0:[];return this[e?"find":"filter"](function(e){for(var i in t){if(t[i]!==e.get(i))return false}return true})},findWhere:function(t){return this.where(t,true)},sort:function(t){if(!this.comparator)throw new Error("Cannot sort a set without a comparator");t||(t={});if(i.isString(this.comparator)||this.comparator.length===1){this.models=this.sortBy(this.comparator,this)}else{this.models.sort(i.bind(this.comparator,this))}if(!t.silent)this.trigger("sort",this,t);return this},pluck:function(t){return i.invoke(this.models,"get",t)},fetch:function(t){t=t?i.clone(t):{};if(t.parse===void 0)t.parse=true;var e=t.success;var r=this;t.success=function(i){var s=t.reset?"reset":"set";r[s](i,t);if(e)e(r,i,t);r.trigger("sync",r,i,t)};q(this,t);return this.sync("read",this,t)},create:function(t,e){e=e?i.clone(e):{};if(!(t=this._prepareModel(t,e)))return false;if(!e.wait)this.add(t,e);var r=this;var s=e.success;e.success=function(t,i){if(e.wait)r.add(t,e);if(s)s(t,i,e)};t.save(null,e);return t},parse:function(t,e){return t},clone:function(){return new this.constructor(this.models)},_reset:function(){this.length=0;this.models=[];this._byId={}},_prepareModel:function(t,e){if(t instanceof p)return t;e=e?i.clone(e):{};e.collection=this;var r=new this.model(t,e);if(!r.validationError)return r;this.trigger("invalid",this,r.validationError,e);return false},_addReference:function(t,e){this._byId[t.cid]=t;if(t.id!=null)this._byId[t.id]=t;if(!t.collection)t.collection=this;t.on("all",this._onModelEvent,this)},_removeReference:function(t,e){if(this===t.collection)delete t.collection;t.off("all",this._onModelEvent,this)},_onModelEvent:function(t,e,i,r){if((t==="add"||t==="remove")&&i!==this)return;if(t==="destroy")this.remove(e,r);if(e&&t==="change:"+e.idAttribute){delete this._byId[e.previous(e.idAttribute)];if(e.id!=null)this._byId[e.id]=e}this.trigger.apply(this,arguments)}});var _=["forEach","each","map","collect","reduce","foldl","inject","reduceRight","foldr","find","detect","filter","select","reject","every","all","some","any","include","contains","invoke","max","min","toArray","size","first","head","take","initial","rest","tail","drop","last","without","difference","indexOf","shuffle","lastIndexOf","isEmpty","chain","sample"];i.each(_,function(t){g.prototype[t]=function(){var e=o.call(arguments);e.unshift(this.models);return i[t].apply(i,e)}});var b=["groupBy","countBy","sortBy","indexBy"];i.each(b,function(t){g.prototype[t]=function(e,r){var s=i.isFunction(e)?e:function(t){return t.get(e)};return i[t](this.models,s,r)}});var w=e.View=function(t){this.cid=i.uniqueId("view");t||(t={});i.extend(this,i.pick(t,E));this._ensureElement();this.initialize.apply(this,arguments);this.delegateEvents()};var x=/^(\S+)\s*(.*)$/;var E=["model","collection","el","id","attributes","className","tagName","events"];i.extend(w.prototype,u,{tagName:"div",$:function(t){return this.$el.find(t)},initialize:function(){},render:function(){return this},remove:function(){this.$el.remove();this.stopListening();return this},setElement:function(t,i){if(this.$el)this.undelegateEvents();this.$el=t instanceof e.$?t:e.$(t);this.el=this.$el[0];if(i!==false)this.delegateEvents();return this},delegateEvents:function(t){if(!(t||(t=i.result(this,"events"))))return this;this.undelegateEvents();for(var e in t){var r=t[e];if(!i.isFunction(r))r=this[t[e]];if(!r)continue;var s=e.match(x);var n=s[1],a=s[2];r=i.bind(r,this);n+=".delegateEvents"+this.cid;if(a===""){this.$el.on(n,r)}else{this.$el.on(n,a,r)}}return this},undelegateEvents:function(){this.$el.off(".delegateEvents"+this.cid);return this},_ensureElement:function(){if(!this.el){var t=i.extend({},i.result(this,"attributes"));if(this.id)t.id=i.result(this,"id");if(this.className)t["class"]=i.result(this,"className");var r=e.$("<"+i.result(this,"tagName")+">").attr(t);this.setElement(r,false)}else{this.setElement(i.result(this,"el"),false)}}});e.sync=function(t,r,s){var n=T[t];i.defaults(s||(s={}),{emulateHTTP:e.emulateHTTP,emulateJSON:e.emulateJSON});var a={type:n,dataType:"json"};if(!s.url){a.url=i.result(r,"url")||M()}if(s.data==null&&r&&(t==="create"||t==="update"||t==="patch")){a.contentType="application/json";a.data=JSON.stringify(s.attrs||r.toJSON(s))}if(s.emulateJSON){a.contentType="application/x-www-form-urlencoded";a.data=a.data?{model:a.data}:{}}if(s.emulateHTTP&&(n==="PUT"||n==="DELETE"||n==="PATCH")){a.type="POST";if(s.emulateJSON)a.data._method=n;var o=s.beforeSend;s.beforeSend=function(t){t.setRequestHeader("X-HTTP-Method-Override",n);if(o)return o.apply(this,arguments)}}if(a.type!=="GET"&&!s.emulateJSON){a.processData=false}if(a.type==="PATCH"&&k){a.xhr=function(){return new ActiveXObject("Microsoft.XMLHTTP")}}var h=s.xhr=e.ajax(i.extend(a,s));r.trigger("request",r,h,s);return h};var k=typeof window!=="undefined"&&!!window.ActiveXObject&&!(window.XMLHttpRequest&&(new XMLHttpRequest).dispatchEvent);var T={create:"POST",update:"PUT",patch:"PATCH","delete":"DELETE",read:"GET"};e.ajax=function(){return e.$.ajax.apply(e.$,arguments)};var $=e.Router=function(t){t||(t={});if(t.routes)this.routes=t.routes;this._bindRoutes();this.initialize.apply(this,arguments)};var S=/\((.*?)\)/g;var H=/(\(\?)?:\w+/g;var A=/\*\w+/g;var I=/[\-{}\[\]+?.,\\\^$|#\s]/g;i.extend($.prototype,u,{initialize:function(){},route:function(t,r,s){if(!i.isRegExp(t))t=this._routeToRegExp(t);if(i.isFunction(r)){s=r;r=""}if(!s)s=this[r];var n=this;e.history.route(t,function(i){var a=n._extractParameters(t,i);n.execute(s,a);n.trigger.apply(n,["route:"+r].concat(a));n.trigger("route",r,a);e.history.trigger("route",n,r,a)});return this},execute:function(t,e){if(t)t.apply(this,e)},navigate:function(t,i){e.history.navigate(t,i);return this},_bindRoutes:function(){if(!this.routes)return;this.routes=i.result(this,"routes");var t,e=i.keys(this.routes);while((t=e.pop())!=null){this.route(t,this.routes[t])}},_routeToRegExp:function(t){t=t.replace(I,"\\$&").replace(S,"(?:$1)?").replace(H,function(t,e){return e?t:"([^/?]+)"}).replace(A,"([^?]*?)");return new RegExp("^"+t+"(?:\\?([\\s\\S]*))?$")},_extractParameters:function(t,e){var r=t.exec(e).slice(1);return i.map(r,function(t,e){if(e===r.length-1)return t||null;return t?decodeURIComponent(t):null})}});var N=e.History=function(){this.handlers=[];i.bindAll(this,"checkUrl");if(typeof window!=="undefined"){this.location=window.location;this.history=window.history}};var R=/^[#\/]|\s+$/g;var O=/^\/+|\/+$/g;var P=/msie [\w.]+/;var C=/\/$/;var j=/#.*$/;N.started=false;i.extend(N.prototype,u,{interval:50,atRoot:function(){return this.location.pathname.replace(/[^\/]$/,"$&/")===this.root},getHash:function(t){var e=(t||this).location.href.match(/#(.*)$/);return e?e[1]:""},getFragment:function(t,e){if(t==null){if(this._hasPushState||!this._wantsHashChange||e){t=decodeURI(this.location.pathname+this.location.search);var i=this.root.replace(C,"");if(!t.indexOf(i))t=t.slice(i.length)}else{t=this.getHash()}}return t.replace(R,"")},start:function(t){if(N.started)throw new Error("Backbone.history has already been started");N.started=true;this.options=i.extend({root:"/"},this.options,t);this.root=this.options.root;this._wantsHashChange=this.options.hashChange!==false;this._wantsPushState=!!this.options.pushState;this._hasPushState=!!(this.options.pushState&&this.history&&this.history.pushState);var r=this.getFragment();var s=document.documentMode;var n=P.exec(navigator.userAgent.toLowerCase())&&(!s||s<=7);this.root=("/"+this.root+"/").replace(O,"/");if(n&&this._wantsHashChange){var a=e.$('<iframe src="javascript:0" tabindex="-1">');this.iframe=a.hide().appendTo("body")[0].contentWindow;this.navigate(r)}if(this._hasPushState){e.$(window).on("popstate",this.checkUrl)}else if(this._wantsHashChange&&"onhashchange"in window&&!n){e.$(window).on("hashchange",this.checkUrl)}else if(this._wantsHashChange){this._checkUrlInterval=setInterval(this.checkUrl,this.interval)}this.fragment=r;var o=this.location;if(this._wantsHashChange&&this._wantsPushState){if(!this._hasPushState&&!this.atRoot()){this.fragment=this.getFragment(null,true);this.location.replace(this.root+"#"+this.fragment);return true}else if(this._hasPushState&&this.atRoot()&&o.hash){this.fragment=this.getHash().replace(R,"");this.history.replaceState({},document.title,this.root+this.fragment)}}if(!this.options.silent)return this.loadUrl()},stop:function(){e.$(window).off("popstate",this.checkUrl).off("hashchange",this.checkUrl);if(this._checkUrlInterval)clearInterval(this._checkUrlInterval);N.started=false},route:function(t,e){this.handlers.unshift({route:t,callback:e})},checkUrl:function(t){var e=this.getFragment();if(e===this.fragment&&this.iframe){e=this.getFragment(this.getHash(this.iframe))}if(e===this.fragment)return false;if(this.iframe)this.navigate(e);this.loadUrl()},loadUrl:function(t){t=this.fragment=this.getFragment(t);return i.any(this.handlers,function(e){if(e.route.test(t)){e.callback(t);return true}})},navigate:function(t,e){if(!N.started)return false;if(!e||e===true)e={trigger:!!e};var i=this.root+(t=this.getFragment(t||""));t=t.replace(j,"");if(this.fragment===t)return;this.fragment=t;if(t===""&&i!=="/")i=i.slice(0,-1);if(this._hasPushState){this.history[e.replace?"replaceState":"pushState"]({},document.title,i)}else if(this._wantsHashChange){this._updateHash(this.location,t,e.replace);if(this.iframe&&t!==this.getFragment(this.getHash(this.iframe))){if(!e.replace)this.iframe.document.open().close();this._updateHash(this.iframe.location,t,e.replace)}}else{return this.location.assign(i)}if(e.trigger)return this.loadUrl(t)},_updateHash:function(t,e,i){if(i){var r=t.href.replace(/(javascript:|#).*$/,"");t.replace(r+"#"+e)}else{t.hash="#"+e}}});e.history=new N;var U=function(t,e){var r=this;var s;if(t&&i.has(t,"constructor")){s=t.constructor}else{s=function(){return r.apply(this,arguments)}}i.extend(s,r,e);var n=function(){this.constructor=s};n.prototype=r.prototype;s.prototype=new n;if(t)i.extend(s.prototype,t);s.__super__=r.prototype;return s};p.extend=g.extend=$.extend=w.extend=N.extend=U;var M=function(){throw new Error('A "url" property or function must be specified')};var q=function(t,e){var i=e.error;e.error=function(r){if(i)i(t,r,e);t.trigger("error",t,r,e)}};return e});
//# sourceMappingURL=backbone-min.map
/**
 * Utilities for seg component
 * 
 * 
 */
( function( $, window, document ){
	
	var Misc = function( a ){
		
		// attributes or global vars here
		
	};
	
	Misc.prototype = {
			
			/**
			 * Inializes the functions when DOM ready
			 */			
			initialize: function(){

				this.closeModalWindow();
				// this.renderTabs();
				// this.renderAccordeon();
				// this.initTimePicker();
				// this.calculateYear();
			}
			
			/**
			 *  Serialize form into json format
			 *  
			 *  @param { string } name class or id of the html element to embed the loader
			 *  @return { object } form into json
			 *  
			 */
		,	formToJson: function( selector ){
			
				var o = {};
			    var a = $( selector ).serializeArray();

			    $('selector input:disabled').each(function () { 
		            a.push({ name: this.name, value: $(this).val() });
		        });
			    
			    $.each( a, function() {
			        if ( o[ this.name ] !== undefined ) {
			            if ( ! o[this.name].push ) {
			                o[ this.name ] = [ o[ this.name ] ];
			            }
			            
			            o[ this.name ].push( this.value || '' );
			            
			        } else {
			            o[ this.name ] = this.value || '';
			        }
			    });
			    
			    return o;	
				
			}
		
	       /**
	         * Helps in the process of making a ajax requests
	         *
	         * @param { object } Options for configuring the ajax request
	         * @param { object } data object to be sent
	         */
		,	ajaxHandler: function( options, data ) {
	
				var result
				,   defaults = {
						type: 'post'
					,   url: 'index.php'
					,   data: data
					,   async: false
					,   success: function( data ) {
							result = data;
						}

					,   error: function ( XMLHttpRequest, textStatus, errorThrown ) {
							console.log( "error :" + XMLHttpRequest.responseText );
						}
				}
	
				// Merge defaults and options
				options = $.extend( {}, defaults, options );

				// Do the ajax request
				$.ajax( options );

				// Return the response object
				return result;
	
	        }

	        /**
            * Given an array of required fields, this function
            * checks whether the second argument have them
            */
        ,   validateEmptyFields: function( required, objectData, errors ) {


                $.each( required, function( key, value ) {

                    if ( objectData[ value ] == null || objectData[ value ] == "" ) {

                        errors.push( value );

                    }

                });

                return errors;

            }

            /**
			* Given an array of required fields, this function
			* checks whether the second argument have them
			*/
		,   validateEmptyObjectAttrs: function( _object, errors ) {

				$.each( _object, function( key, value ) {

					if ( value == null || value == "" ) {

						errors.push( key );

					}

				});

				return errors;

			}

            /**
			*
			* Validate only numbers
			* @param { string } the string to validate
			* 
			*/
		,	justNumbers: function( value ){

				var pattern = /^\d+$/
				, 	exp = new RegExp( pattern );


				if( typeof value == 'undefined' )
					return false;


				return exp.test( value );

			}

			/**
			*
			* Validate only letters
			* @param { string } the string to validate
			* 
			*/
		,	justLetters: function( value ){

				var pattern = /^[ñA-Za-z _]*[ñA-Za-z][ñA-Za-z _]*$/
				, 	exp = new RegExp( pattern );


				if( typeof value == 'undefined' )
					return false;


				return exp.test( value );

			}

			/**
			* Converts latin string to anglo
			*
			* @param { string } the string to be sanitized
			* @param { bool } numbers are allowed or not
			* @param { bool } special characters are allowed or not
			* @param { bool } blank spaces are allowed or not
			* @param { string } blank spaces are allowed or not
			* @param { string } blank spaces are replaced to
			* @return { string } the string sanitized
			*
			*/
		,	latinToAnglo: function( value, allowNumbers, allowSpecial, allowSpaces, replace ){

				if( typeof replace == 'undefined' || replace == null )
					replace = '';

				value = value.replace(/[ÀÁÂÃÄÅ]/,"A");
			    value = value.replace(/[àáâãäå]/,"a");
			    value = value.replace(/[ÈÉÊË]/,"E");
			    value = value.replace(/[èéêë]/,"e");
			    value = value.replace(/[íìîï]/,"i");
			    value = value.replace(/[ÍÌÎÏ]/,"I");
			    value = value.replace(/[óòôö]/,"o");
			    value = value.replace(/[ÒÓÔÖ]/,"O");
			    value = value.replace(/[úùûü]/,"u");
			    value = value.replace(/[ÚÙÛÜ]/,"U");
			    value = value.replace(/[ç]/g,"c");
			    value = value.replace(/[Ç]/,"C");
			    value = value.replace(/[ñ]/,"n");
			    value = value.replace(/[Ñ]/,"N");

			    if( ! allowNumbers )
			    	value = value.replace(/[1234567890]/g, '');

			    if( ! allowSpecial )
			    	value = value.replace(/[&\/\\#,+=\-\[\]()$~%.'":*?¿!¡^ºª<>{}´`¨]/g, '' );

			    if( ! allowSpaces )
			    	value = value.replace( /[  ]/g, replace);
		
				return value;
			}

        	/**
            * Check whether an string is a correct email
            * @param { str } String to test
            * @return { bool }
            */
        ,   isEmail: function( string ) {

                var emailExpression = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                return emailExpression.test( string );
            }

            /**
            * Sets a countdown
            *
            * @param { object } arguments to set: count, limit, selector, callback
            * @return { function } the callback passed, return otherwise
            *  
            */
        ,	setCountdown: function( args ){

				var counter=setInterval( reverse, 1000); //1000 will  run it every 1 second

				function reverse(){
					
					args.count = args.count - 1;

					if ( args.count <= args.limit	 ) {
						
						clearInterval( counter);

						if ( args.callback !== null ) {

                            args.callback.call();
                        }

						return;
					}

					$( args.selector ).text( args.count );
				}
        	}

        	/**
        	* Sets a countdown a display the text in a HTML Object, triggers a function callback when reaches the limit
        	*
        	* @param { object } args with data {
				
						duration: ( int ) duration in milliseconds
					,	interval: ( int ) interval of the count down in milliseconds
					,	limit: ( int ) limit until callback executes
					,	selector: ( string ) jQuery selector string
					,	callback: ( function ) function callback
        		}
        	*
        	* @return { function } Callback function
        	*
        	*/
        ,	humanCountDown: function( args ){

        		var counter = setInterval( humanReverse, 1000)
				,	_this = this;

				function humanReverse(){

					args.duration = args.duration - args.interval;

					var mom = moment( args.duration ).format( 'mm:ss' );

					if ( args.duration < args.limit ) {
						
						clearInterval( counter);

						if ( args.callback !== null ) {

                            args.callback.call();
                        }

						return;
					}

        			$( args.selector ).text( mom );
				}
				
        	}

    	,	showNotification: function( type, message, time, callback ){
			
				
	    		$( '.global-notification' ).removeClass( 'error, success, warning, info' );
	    		$( '.global-notification' ).addClass( type );
	    		$( '.global-notification' ).text( message );
	    		$( '.global-notification' ).fadeIn();

	    		this.closeOnClickOut( '.global-notification' );
				
				if( time != 0 ){

					if( typeof callback != 'undefined' ){
						setTimeout( function(){ 
							$( '.global-notification' ).fadeOut();
							callback.call();
						}, time )
						return;
					}

					setTimeout( function(){ $( '.global-notification' ).fadeOut(); }, time )
					return;
	    			
				}
    		}

    		/**
        	* validates if object is empty
        	*
        	*/
        ,	isEmptyObject: function( obj ){

        		// Speed up calls to hasOwnProperty
				var hasOwnProperty = Object.prototype.hasOwnProperty;

			    // null and undefined are "empty"
			    if (obj == null) return true;

			    // Assume if it has a length property with a non-zero value
			    // that that property is correct.
			    if (obj.length && obj.length > 0)    return false;
			    if (obj.length === 0)  return true;

			    // Otherwise, does it have any properties of its own?
			    // Note that this doesn't handle
			    // toString and toValue enumeration bugs in IE < 9
			    for (var key in obj) {
			        if (hasOwnProperty.call(obj, key)) return false;
			    }

			    return true;
        	}

			/**
			* Hides an element when it is clicked outiside
			* @param { string } string for the jQuery selector like: ".my-class"
			* @return { null }
			*/
		,   closeOnClickOut: function( selector, callback ) {

				$( document ).mouseup( function( e ) {

					if ( ! $( selector ).is( ":visible" ) ) {
						return;
					}

					if ( $( selector ).has( e.target ).length === 0 ) {

						$( selector ).hide();
						if ( callback != null ) {
							callback.call();
						}
					}
				});

			}

			/**
			* Sets dots in numbers
			*
			* @param { numeric } the number to be changed
			* @param { numeric } how many decimals do you want to display for? e.g 1.000.00 ( two decimals )
			* @param { string } decimals character separator
			* @param { string } thousands character separator
			* @return { string } the number formatted
			*
			*/
		,	numberDots: function( num, decimals, decimalSeparator, thousandSeparator ){

				num = parseInt( num );

				var number = new String( num );

				var result = '';

				if( typeof decimals == 'undefined' )
					decimals = 2;

				if( typeof decimalSeparator == 'undefined' )
					decimalSeparator = '.';

				if( typeof thousandSeparator == 'undefined' )
					thousandSeparator = '.';



				while( number.length > 3 ){

				 	result = thousandSeparator + number.substr(number.length - 3) + result;

				 	number = number.substring(0, number.length - 3);

				}

				result = number + result;

				if( decimals != 0 ){

					result += decimalSeparator

					for( var i = 0; i < decimals; i++ ){
						result += '0';
					}
				}

				return result;

			}

			/**
			* Sets up the calendar
			*
			* Note: jqueryUI is required
			*
			* @param { string } the jQuery selector to attach the calendar
			*
			*/
		,	setCalendar: function( selector, options ){

				var dOptions = {

					monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
					'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
					monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
					'Jul','Ago','Sep','Oct','Nov','Dic'],
					dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
					dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
					dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
					firstDay: 1,
					prevText: '&#x3c;Ant', prevStatus: '',
					prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
					nextText: 'Sig&#x3e;', nextStatus: '',
					nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
					currentText: 'Hoy', currentStatus: '',
					todayText: 'Hoy', todayStatus: '',
					clearText: '-', clearStatus: '',
					closeText: 'Cerrar', closeStatus: '',
					yearStatus: '', monthStatus: '',
					weekText: 'Sm', weekStatus: '',
					dayStatus: 'DD d MM',
					defaultStatus: '',
					isRTL: false,
					dateFormat: "yy-mm-dd",
					showOn: "both",
					buttonImage: url + "images/calendario-mini.png",
					buttonImageOnly: true,
					changeYear: true,
					changeMonth: true

				};


				if( typeof options != 'undefined' )
					$.extend( dOptions, options );

				$( selector ).datepicker( dOptions );

			}

			/**
			* Sets the time picker
			*
			* Note: Requires jqueryUI.timepicker https://fgelinas.com/code/timepicker/
			*
			* @param { string } the jquery selector to set the timepicker
			*
			*/
		,	setTimePicker: function( selector ){

				$( selector ).timepicker({
				    // Options
				    timeSeparator: ':',
				    showLeadingZero: true,
				    showPeriodLabels: false,
				    // Localization
				    hourText: 'Hora',
				    minuteText: 'Minutos',
				    amPmText: ['AM', 'PM'],
				    myPosition: 'left top',
				    atPosition: 'left bottom',
				    hours: {
				        starts: 0,                // First displayed hour
				        ends: 23                  // Last displayed hour
				    },
				    minutes: {
				        starts: 0,                // First displayed minute
				        ends: 45,                 // Last displayed minute
				        interval: 15,              // Interval of displayed minutes
				        manual: []                // Optional extra entries for minutes
				    },
				    rows: 4,                      // Number of rows for the input tables, minimum 2, makes more sense if you use multiple of 2
				    showHours: true,              // Define if the hours section is displayed or not. Set to false to get a minute only dialog
				    showMinutes: true,
				    // buttons
				    showCloseButton: false,       // shows an OK button to confirm the edit
				    closeButtonText: 'Done',      // Text for the confirmation button (ok button)
				    showNowButton: false,         // Shows the 'now' button
				    nowButtonText: 'Ahora',         // Text for the now button
				    showDeselectButton: false,    // Shows the deselect time button
				    deselectButtonText: 'Deseleccionar' // Text for the deselect button

				});
			}

			/**
			* Clear all window intervals started previously
			*
			*/
		,	clearAllIntervals: function(){

				var interval_id = window.setInterval("", 9999);
				// Get a reference to the last interval +1
				for (var i = 1; i < interval_id; i++){
					console.log( i );
					window.clearInterval( i );
				}
			}

			/**
			* Show modal window and render a template, runs a callback function too
			*
			* @param { object } configuration object {
				
				  @param { string } modal header title
				, @param { string } html content
				, @param { numeric } width
				, @param { numeric } height
				, @param { function } a callback function to be called after show
			}
			*
			*/
		,	showModalWindow: function( config ){

				$( '.em-modal-box .header .modal-title' ).text( config.title );
				$( '.em-modal-box .body' ).html( config.content );

				// set dimensions
				if( typeof config.width != 'undefined' ){
					$( '.em-modal-box' ).width( config.width );
					$( '.em-modal-box' ).css( 'margin-left', '-' + ( config.width / 2 ) + 'px' );
				}

				if( typeof config.height != 'undefined' ){
					$( '.em-modal-box' ).height( config.height );
					$( '.em-modal-box' ).css( 'min-height', config.height );
					$( '.em-modal-box' ).css( 'margin-top', '-' + ( config.height / 2 ) + 'px' );
				}else{
					$( '.em-modal-box' ).css( 'height', 'auto' );
					$( '.em-modal-box' ).css( 'min-height', 'auto' );
					var height = $( '.em-modal-box' ).height();
					
					$( '.em-modal-box' ).css( 'margin-top', '-' + ( height / 2 ) + 'px' );
				}

				$( '.em-over-screen' ).fadeIn();
				$( '.em-modal-box' ).fadeIn();


				// runs a callback after show
				if( typeof callback != 'undefined' ){
					callback.call();
				}
			}

			/**
			* Close modal window 
			*
			*/
		,	closeModalWindow: function(){

				$( document ).ready( function(){
					$( 'body' ).delegate( '.close-modal-button', 'click', function( e ){

						e.preventDefault();

						$( '.em-modal-box .body' ).html( '' );
						$( '.em-over-screen' ).fadeOut();
						$( '.em-modal-box' ).fadeOut();

					});

					$( '.em-over-screen' ).click( function( e ){

						$( '.em-modal-box .body' ).html( '' );
						$( '.em-over-screen' ).fadeOut();
						$( '.em-modal-box' ).fadeOut();

					});
				});
				

				// Esc key press
				$( document ).keyup( function( e ){

					var keyCode = e.which || e.keyCode;

					if( keyCode == 27 ){
						$( '.em-modal-box .body' ).html( '' );
						$( '.em-over-screen' ).fadeOut();
						$( '.em-modal-box' ).fadeOut();
					}
				});
			}

			/**
			* Close modal window 
			*
			*/
		,	_closeModalWindow: function(){

				$( '.em-modal-box .body' ).html( '' );
				$( '.em-over-screen' ).fadeOut();
				$( '.em-modal-box' ).fadeOut();
			}

			/**
			*
			*
			*/
		,	htmlEntities: function( str ) {
			    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
			}

			/**
			* Parse a json string to JS format
			*
			*/
		,	jsonToObject: function( text ){


				text = text.replace(/'/g, '"');

				var object = JSON.parse( text, function (key, value) {
					    var type;
				    if (value && typeof value === 'object') {
				        type = value.type;
				        if (typeof type === 'string' && typeof window[type] === 'function') {
				            return new (window[type])(value);
				        }
				    }
				    return value;
				});

				return object;
			}

			/**
			* Gets any elements from array in random and withot repeat
			*
			* @param { array } array of elements
			* @param { numeric } number of items to be get
			*
			* 
			*/
		,	randomFrom: function ( array, n ) {
			    var at = 0;
			    var tmp, current, top = array.length;

			    if(top) while(--top && at++ < n) {
			        current = Math.floor(Math.random() * (top - 1));
			        tmp = array[current];
			        array[current] = array[top];
			        array[top] = tmp;
			    }

			    return array.slice(-n);
			}

			/**
			* Customize some functions from Date
			*
			*/
		,	customizeDate: function(){

				Date.prototype.getHoursTwoDigits = function(){
				    var retval = this.getHours();
				    if (retval < 10){
				        return ("0" + retval.toString());
				    }else {
				        return retval.toString();
				    }
				}

				Date.prototype.getMinutesTwoDigits = function(){
				    var retval = this.getMinutes();
				    if (retval < 10){
				        return ("0" + retval.toString());
				    }else {
				        return retval.toString();
				    }
				}
			}

			/**
			*  Sets a loading spinner in a box
			* @param { type } type description
			* @return { type } return description
			*
			*/
		,	setSpinner: function( selector, text ){

				var spinner = '<div class="wrapper-spinner"><p class="loader-text">' + text + '</p><div id="floatingCirclesG"><div class="f_circleG" id="frotateG_01"></div><div class="f_circleG" id="frotateG_02"></div><div class="f_circleG" id="frotateG_03"></div><div class="f_circleG" id="frotateG_04"></div><div class="f_circleG" id="frotateG_05"></div><div class="f_circleG" id="frotateG_06"></div><div class="f_circleG" id="frotateG_07"></div><div class="f_circleG" id="frotateG_08"></div></div></div>';

				console.log( selector );

				// attach the spinner to the selector
				$( selector ).html( spinner );

			}

			/**
			* Removes the loading spinner and trigger a callback
			* @param { type } type description
			* @return { type } return description
			*
			*/
		,	removeSpinner: function( time, callback ){

				// if time > 0 set a time out and call the callback
				if( time > 0 ){
					if( typeof callback != 'undefined' ){
						setTimeout( function(){ $('.wrapper-spinner').remove(); callback.call(); }, time );
						return;
					}
				}

				if( typeof callback != 'undefined' ){
					$('.wrapper-spinner').remove();
					callback.call();
				}
			}

			/**
			* Redirect to an specific url or refresh the page
			* @param { string } the url to be redirect to
			*
			*/
		,	redirect: function( url ){

				if( typeof url != '' ){
					window.location.reload();
				}

				window.location = url;


			}

			/**
			* Parses string formatted as YYYY-MM-DD to a Date object.
			* If the supplied string does not match the format, an 
			* invalid Date (value NaN) is returned.
			* @param {string} dateStringInRange format YYYY-MM-DD, with year in
			* range of 0000-9999, inclusive.
			* @return {Date} Date object representing the string.
			*/

		,	parseISO8601: function ( dateStringInRange ) {
				var isoExp = /^\s*(\d{4})-(\d\d)-(\d\d)\s*$/,
				date = new Date(NaN), month,
				parts = isoExp.exec(dateStringInRange);

				if(parts) {
					month = +parts[2];
					date.setFullYear(parts[1], month - 1, parts[3]);
					
					if(month != date.getMonth() + 1) {
						date.setTime(NaN);
					}
				}
				return date;
			}


			/**
			* Copy a text to clipboard
			*
			*/
		,	copyToClipboard: function( id ){

				var client = new ZeroClipboard();
				client.clip( document.getElementById( id ) );

				client.on( 'aftercopy', function ( event ) {
					if ( event.success['text/plain'] ) {

						$( '#' + id ).text( 'Copiado!' );
					}
					else {
						$( '#' + id ).removeClass( 'green' );
						$( '#' + id ).addClass( 'red' );
						$( '#' + id ).text( 'Falló la copia!' );
					}
				} );

			}

			/**
			* Searches an object into an array by key
			*
			* @param { array } array of objects (haystack)
			* @param { string } the key to filter
			* @param { mixed } the value to filter (needle)
			* @return { mixed } false if it's not found, object or array of objects otherwise
			*
			*/
		,	searchObject: function( array, key, value ){

				var i = 0;

				var result = $.grep( array, function( element, index ){

					if( element[key] == value ){

						i = index;

						return ( element[key] == value );
					}
				});

				//console.log(result);

				if (result.length == 0) {
					return false;

				} else if (result.length == 1) {
					
					return {
						element: result[0],
						indexOf: i
					}

				} else {
				  
				  return result;
				}
			}

        ,	initDatePicker: function( blockdates ){


        		$('.calendar').datepicker({
					dateFormat: "yy-mm-dd",
					firstDay: 1,
					dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
					dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
					monthNames:["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
					"Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
					monthNamesShort:["Ene", "Feb", "Mar", "Abr", "May", "Jun",
					"Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
					changeMonth: true,
					changeYear: true,
					yearRange: "-99:+14",
					onSelect: function (argument) {

						var fecha = $( "#fecha" ).val(argument);

					},
					beforeShowDay: function(date){
				        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
				        return [ blockdates.indexOf(string) == -1 ]
				    }
				});

				

				
        	}

        ,	triggerCalendar: function( selector ){

					$(selector).datepicker( "show" );

        	}	

        ,	renderTabs: function(){
        		$( "#tabs" ).tabs();
        	}

        ,	renderAccordeon: function(){
        		$( ".accordion" ).accordion({
        			heightStyle: "content"
        		});
        	}

        ,	initTimePicker: function(){
        		$('.time').timepicker({
        			 // Localization
				    hourText: 'Hora',             // Define the locale text for "Hours"
				    minuteText: 'Minutos',         // Define the locale text for "Minute"
				    amPmText: ['AM', 'PM'],       // Define the locale text for periods
				    defaultTime: '12:34'
        		});
        	}

        ,	calculateYear: function(){

        		$('.date').change(function() {

        			var currentDate = new Date();
        			var edad = $('.date').val();

        			var year = edad.split('-');

        			var currentDateYear = currentDate.getFullYear();

        			var old = currentDateYear - year[0];


        			$( '#datos-personales-form' ).find( 'input[name="edad"]' ).val( old );
        		});

        	}

        ,	dateFormat: function( strdata ){



        		var meses = {
        			'01' : 'Enero',
        			'02' : 'Febrero',
        			'03' : 'Marzo',
        			'04' : 'Abril',
        			'05' : 'Mayo',
        			'06' : 'Junio',
        			'07' : 'Julio',
        			'08' : 'Agosto',
        			'09' : 'Septiembre',
        			'10' : 'Octubre',
        			'11' : 'Noviembre',
        			'12' : 'Diciembre'
        		};

        		var fecha = strdata.split('-');
        		
			    var texto = fecha[2] + ' de ' + meses[ fecha[1] ] + ' del ' + fecha[0];
			    
			    
			    return texto;	

        	}
	};
		
	window.Misc = new Misc();
	window.Misc.initialize();
		
})( jQuery, this, this.document, undefined );
/**
* Anuncio Model
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	MapModel = Backbone.Model.extend({
	        defaults: {
	        }

	    ,   initialize: function(){
	            
	        }


	     	/**
	        * Instance controller to send mail
	        *
	        */
	    ,	getZona: function( callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_zonas'
					,	task: 'zonas.getZonas'

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

    });

})( jQuery, this, this.document, this.Misc, undefined );
/**
* Google Maps View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var MapView = {};

	// Extends my object from Backbone events
	MapView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'keyup #direccion': 'renderAddressOnMap',
				'click .button-vertice': 'addVertice',
				'click .pac-input': 'initProductMap',
				'click .add-zone': 'renderZone',
				'click .delete-item': 'deleteVertice'

			}

		,	view: this

		,	mapOptions: {
				center: new google.maps.LatLng( 4.626730702566083,-74.080810546875  ),
				zoom: 10,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}



		,	vertice : 0

		,	uniqueId : 1

		,	map: {}

		,	geocoder: {}

		,	initialize: function(){

				_.bindAll(
					this, 
					'initMap',
					'initProductMap',
					'addListeners',
					'onClickMap',
					'renderAddressOnMap',
					'placeMarker',
					'renderZone',
					'deleteVertice',
					'initEditMap',
					'initEdit',
					'addVertice'
				);

				this.mapa = new MapModel();

				if( $('#map_canvas').length ){
					this.initMap();
					this.addListeners();
				}

				
				if( $('#zone-map-canvas').length ){
					this.mapZone = new google.maps.Map(document.getElementById('zone-map-canvas'), this.mapOptions);
				}	
					
				google.maps.event.addDomListener(window, 'load', this.initProductMap());

				this.triangleCoords = [];
				this.markers = [];

				this.bermudaTriangle;



				if( $('#zone-map-canvas-edit').length ){
					this.mapZone = new google.maps.Map(document.getElementById('zone-map-canvas-edit'), this.mapOptions);
					this.initEditMap();
					// this.addListeners();
					
				}
				
			}

		,	initEdit: function(){

				var _this = this;


				if (this.triangleCoords.length < 3)
					utilities.showNotification('error', 'Debe añadir minímo tres puntos en el mapa para pintar la zona.',2000);

				var center = $('.hidden').val().split(',');

				this.mapOptions = {
					center: new google.maps.LatLng( center[1], center[0] ),
					zoom: 13,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};


               	if (_this.vertice < 3)
					$('.add-zone').css({'display':'none'});


				this.mapZone = new google.maps.Map(document.getElementById('zone-map-canvas-edit'), this.mapOptions);

				var triangleCoords = [];

				$.each(this.triangleCoords, function(index, val) {


					triangleCoords.push(new google.maps.LatLng(val.D, val.k));

					var latLng = new google.maps.LatLng(val.D, val.k);

					var marker = new google.maps.Marker({
						map: _this.mapZone,
						position: latLng
					});

					marker.id = _this.uniqueId;
		            _this.uniqueId++;

		            _this.markers.push(marker);
				});

				if ( this.bermudaTriangle != undefined ) {
					this.bermudaTriangle.setMap(null);
				}


				// Construct the polygon.
				this.bermudaTriangle = new google.maps.Polygon({
					paths: triangleCoords,
					strokeColor: $('#color').val(),
					strokeOpacity: 0.8,
					strokeWeight: 3,
					fillColor: $('#color').val(),
					fillOpacity: 0.35
				});
				

				this.bermudaTriangle.setMap(this.mapZone);

				$('#zone').val('1');


			}


		,	initEditMap: function(){


					var _this = this;

					var center = $('.hidden').val().split(',');

					this.mapOptions = {
						center: new google.maps.LatLng( center[1], center[0] ),
						zoom: 13,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};

					this.mapZone = new google.maps.Map(document.getElementById('zone-map-canvas-edit'), this.mapOptions);


					var triangleCoords = [];


					$('.hidden').each(function(){

					    var coordinates = $(this).val().split(',');

					    var latLng = new google.maps.LatLng(coordinates[1], coordinates[0]);

					    var marker = new google.maps.Marker({
							map: _this.mapZone,
							position: latLng
						});

						marker.id = _this.uniqueId;
			            _this.uniqueId++;

			            _this.markers.push(marker);

						triangleCoords.push(latLng);

						_this.triangleCoords.push(new google.maps.LatLng(coordinates[0], coordinates[1]));

						var address = coordinates[1]+','+coordinates[0];

						var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + address + "&sensor=false";

			            jQuery.getJSON(url, function (json) {

			            	_this.vertice = _this.vertice + 1;

			                var formattedAddress = json.results[0].formatted_address;

							var html = '<li class="vertice-'+_this.vertice+'"><input class="pac-input" value="'+formattedAddress+'" id="pac-input'+_this.vertice+'" class="controls" type="text"><a href="#" class="delete-item" data-id="'+_this.vertice+'"><img src="../images/icono-basura.png"></a></li>';

			               	$('.vertices ul').append(html);

			               	if (_this.vertice >= 3)
								$('.add-zone').css({'display':'inline-block'});
			            });

					});

					// Construct the polygon.
					this.bermudaTriangle = new google.maps.Polygon({
						paths: triangleCoords,
						strokeColor: $('#color').val(),
						strokeOpacity: 0.8,
						strokeWeight: 3,
						fillColor: $('#color').val(),
						fillOpacity: 0.35
					});

					this.bermudaTriangle.setMap(this.mapZone);

					$('#zone').val('1');
				
			}

		,	renderZone: function(){



				if (this.triangleCoords.length < 3)
					return utilities.showNotification('error', 'Debe añadir minímo tres puntos en el mapa para pintar la zona.',2000);

				// var map = new google.maps.Map(
				// 	document.getElementById("zone-map-canvas"),
				// 	this.mapOptions
				// );


				var triangleCoords = [
				];

				$.each(this.triangleCoords, function(index, val) {
					triangleCoords.push(new google.maps.LatLng(val.D, val.k));
				});

				if ( this.bermudaTriangle != undefined ) {
					this.bermudaTriangle.setMap(null);
				}

				
				// Construct the polygon.
				this.bermudaTriangle = new google.maps.Polygon({
					paths: triangleCoords,
					strokeColor: $('#color').val(),
					strokeOpacity: 0.8,
					strokeWeight: 3,
					fillColor: $('#color').val(),
					fillOpacity: 0.35
				});
	

				this.bermudaTriangle.setMap(this.mapZone);

				$('#zone').val('1');

			}

			/**
			* Initializes the map on the canvas
			*
			*/
		,	initMap: function(){

				var _self = this;

				if( $( '#latlang' ).length ){

					var latlng = $( '#latlang' ).val();

					if( latlng != '' ){

						latlng = latlng.split(',', 2 );

						var lat = parseFloat(latlng[0]);
						var lng = parseFloat(latlng[1]);

						var latlng = new google.maps.LatLng(lat, lng);

						this.mapOptions.center = latlng;

					}
				}

				this.map = new google.maps.Map(
					document.getElementById("map_canvas"),
					this.mapOptions
				);

				this.marker = new google.maps.Marker({
					map: this.map,
					draggable: false
				});

				this.geocoder = new google.maps.Geocoder();

				this.mapa.getZona( function( data ){

					$.each(data.vertices, function(index, vertice) {

						var triangleCoords = [];

						$.each(vertice.vertices, function(index, ver) {
							
							triangleCoords.push(new google.maps.LatLng(ver.longitud, ver.latitud));

						});

						_self.bermudaTriangle = new google.maps.Polygon({
							paths: triangleCoords,
							strokeColor: vertice.color,
							strokeOpacity: 0.8,
							strokeWeight: 3,
							fillColor: vertice.color,
							fillOpacity: 0.35,
							draggable: true,
							geodesic: true
						});

						_self.bermudaTriangle.setMap(_self.map);

					});

				}, this.onError);

			

				if( typeof latlng == 'object' ){

					_self.geocoder.geocode( { 'latLng': latlng }, function( results, status ) {

						if ( status == google.maps.GeocoderStatus.OK ) {
							if ( results[1] ) {
								_self.marker = new google.maps.Marker({
									position: latlng,
									map: _self.map
								});

								// infowindow.setContent(results[1].formatted_address);
								// infowindow.open(map, marker);
							} else {
								utilities.showNotification( 'warning', 'No se pudo recuperar la localización del anuncio en el mapa.', 3000);
							}
						} else {
							utilities.showNotification( 'error', 'La geocodificación de google maps debido a: ' + status, 3000 );
						}
					});
				}


			}

			/**
			* Initializes the product map
			*
			*/
		,	initProductMap: function( e ){

				this.marker = {};
				
				if (e != undefined) {

					var _this = this;

					if (e == 'algo') {

						var dir = 'Bogotá, Colombia';

						$( '#pac-input'+this.vertice ).val( dir );


						this.marker = new google.maps.Marker({ 
					       	map: this.mapZone,
					       	position: new google.maps.LatLng( 4.598056, -74.07583299999999 ),
					       	draggable: true
				    	});

					}


					google.maps.event.addListener(this.marker, 'dragstart', function() {

			            var a = _this.marker.getPosition();

			            console.log( a );

			            if ( _this.triangleCoords[_this.triangleCoords.length - 1].k == a.D) {

			            	var latLng = new google.maps.LatLng(_this.triangleCoords[_this.triangleCoords.length - 1].k, _this.triangleCoords[_this.triangleCoords.length - 1].D);

							_this.triangleCoords.splice($.inArray(latLng, _this.triangleCoords), 1);
			            }
					    
					});
  

					google.maps.event.addListener(this.marker, 'drag', function() {

						_this.marker.id = _this.uniqueId;
			            _this.uniqueId++;

			            _this.markers.push(_this.marker);

			            var a = _this.marker.getPosition();
			            
					   	_this.getAddress(a.k + ',' + a.D);

						
					});

					google.maps.event.addListener(this.marker, 'dragend', function() {

						_this.marker.id = _this.uniqueId;
			            _this.uniqueId++;

			            _this.markers.push(_this.marker);

			            var a = _this.marker.getPosition();

						_this.triangleCoords.push(new google.maps.LatLng(a.D, a.k));


					});	


					
					var markers = [];

					var mapOptions = {
						center: new google.maps.LatLng( 4.626730702566083,-74.080810546875  ),
						zoom: 11,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};

					var input = (document.getElementById('pac-input'+this.vertice));

					var searchBox = new google.maps.places.SearchBox((input));

					google.maps.event.addListener( this.mapZone, 'click', function(e) {

						_this.marker.setMap(null);

					   	_this.marker = new google.maps.Marker({ 
					       	map: _this.mapZone,
					       	position: e.latLng,
					       	draggable: true
					    });

					   	//Set unique id
			            _this.marker.id = _this.uniqueId;
			            _this.uniqueId++;

			            _this.markers.push(_this.marker);

					   	_this.getAddress(e.latLng.k + ',' + e.latLng.D);
						_this.triangleCoords.push(new google.maps.LatLng(e.latLng.D, e.latLng.k));

						var inputlat = '<input type="hidden" name="coordinates[]" id="lat-hidden-'+_this.vertice+'" value="'+e.latLng.D+','+e.latLng.k+'"/>';

						$('.add-vertice').append(inputlat);


						google.maps.event.addListener(_this.marker, 'dragstart', function() {

				            var a = _this.marker.getPosition();

				            if ( _this.triangleCoords[_this.triangleCoords.length - 1].k == a.D) {

				            	var latLng = new google.maps.LatLng(_this.triangleCoords[_this.triangleCoords.length - 1].k, _this.triangleCoords[_this.triangleCoords.length - 1].D);

								_this.triangleCoords.splice($.inArray(latLng, _this.triangleCoords), 1);
				            }
						    
						});
	  

						google.maps.event.addListener(_this.marker, 'drag', function() {

							_this.marker.id = _this.uniqueId;
				            _this.uniqueId++;

				            _this.markers.push(_this.marker);

				            var a = _this.marker.getPosition();
				            
						   	_this.getAddress(a.k + ',' + a.D);

							
						});

						google.maps.event.addListener(_this.marker, 'dragend', function() {

							_this.marker.id = _this.uniqueId;
				            _this.uniqueId++;

				            _this.markers.push(_this.marker);

				            var a = _this.marker.getPosition();

							_this.triangleCoords.push(new google.maps.LatLng(a.D, a.k));


						});	


					});


					// [START region_getplaces]
					// Listen for the event fired when the user selects an item from the
					// pick list. Retrieve the matching places for that item.
					google.maps.event.addListener(searchBox, 'places_changed', function() {

						_this.places = searchBox.getPlaces();
						
						for (var i = 0, place; place = places[i]; i++) {

							//_this.marker.setMap(null);

							var image = {
								url: place.icon,
								size: new google.maps.Size(71, 71),
								origin: new google.maps.Point(0, 0),
								anchor: new google.maps.Point(17, 34),
								scaledSize: new google.maps.Size(25, 25)
							};

							//Create a marker for each place.
							_this.marker = new google.maps.Marker({
								map: _this.mapZone,
								title: _this.place.name,
								position: _this.place.geometry.location,
								draggable: true
							});

							//Set unique id
				            _this.marker.id = _this.uniqueId;
				            _this.uniqueId++;

				            _this.markers.push(_this.marker);

							_this.triangleCoords.push(new google.maps.LatLng(place.geometry.location.D, place.geometry.location.k));

							var inputlat = '<input type="hidden" name="coordinates[]" id="lat-hidden-'+_this.vertice+'" value="'+place.geometry.location.D+','+place.geometry.location.k+'"/>';

							$('.add-vertice').append(inputlat);


							google.maps.event.addListener(_this.marker, 'dragstart', function() {

					            var a = _this.marker.getPosition();

					            if ( _this.triangleCoords[_this.triangleCoords.length - 1].k == a.D) {

					            	var latLng = new google.maps.LatLng(_this.triangleCoords[_this.triangleCoords.length - 1].k, _this.triangleCoords[_this.triangleCoords.length - 1].D);

									_this.triangleCoords.splice($.inArray(latLng, _this.triangleCoords), 1);
					            }
							    
							});
		  

							google.maps.event.addListener(_this.marker, 'drag', function() {

								_this.marker.id = _this.uniqueId;
					            _this.uniqueId++;

					            _this.markers.push(_this.marker);

					            var a = _this.marker.getPosition();
					            
							   	_this.getAddress(a.k + ',' + a.D);

								
							});

							google.maps.event.addListener(_this.marker, 'dragend', function() {

								_this.marker.id = _this.uniqueId;
					            _this.uniqueId++;

					            _this.markers.push(_this.marker);

					            var a = _this.marker.getPosition();

								_this.triangleCoords.push(new google.maps.LatLng(a.D, a.k));


							});


						}

					});
					

				}

			}
			/**
			* Add the map listeners to the event
			*
			*/
		,	addListeners: function(){

				google.maps.event.addListener( this.map, 'click', this.onClickMap );

				return;
			}


			/**
			* Catch the map address when user clicks on the map
			*
			*/
		,	onClickMap: function( e ){

				this.placeMarker( e.latLng );
				this.getAddress( e.latLng.k + ',' + e.latLng.D );
			}

			/**
			* Render the address inserted by user on the map
			*
			*/
		,	renderAddressOnMap: function( e ){

				var target = e.currentTarget;

				var address = $( target ).val();

				var _self = this;

				this.geocoder.geocode( { 'address': address}, function(results, status) {

					if (status == google.maps.GeocoderStatus.OK) {

						_self.map.setCenter(results[0].geometry.location);
						_self.marker.setPosition( results[0].geometry.location );

					} else {

						return;
					}
				});
			}

			/**
			* Get the read human address from latitud and longitude
			*
			*/
		,	getAddress: function( latLng ){


				var _this = this;

				var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latLng + "&sensor=false";

				//$( '#latlang' ).val( latLng );

	            jQuery.getJSON(url, function (json) {

	                
	                var formattedAddress = json.results[0].formatted_address;
	                // var departamento = json.results[0].address_components[4].long_name;
	                // var city = json.results[0].address_components[3].long_name;
	                // var neighborhood = json.results[1].address_components[0].long_name;
	                
	                formattedAddress = formattedAddress.replace( new RegExp("[,\s]+[ña-zA-Z _]+[,\s]+[ña-zA-Z _]+[ña-zA-Z _]+"), '' );

	                $( '#pac-input'+_this.vertice ).val( formattedAddress );
	            }); 

			}

			/**
			* Place the marker into the map when user clicks on
			*
			*/
		,	placeMarker: function ( location ){

				this.marker.setPosition( location );
				return;
			}

		,	addVertice: function(e){

				e.preventDefault();

				this.vertice = this.vertice + 1;

				var html = '<li class="vertice-'+this.vertice+'"><input class="pac-input" id="pac-input'+this.vertice+'" class="controls" type="text"><a href="#" class="delete-item" data-id="'+this.vertice+'"><img src="../images/icono-basura.png"></a></li>';

				if (this.vertice >= 3)
					$('.add-zone').css({'display':'inline-block'});

				if (this.vertice > 4) {
					alert('No puede añadir más vertices');
				}else{
					$('.vertices ul').append(html);
					this.initProductMap( 'algo' );
				}

			}

		,	deleteVertice: function(e) {

				e.preventDefault();

				var target = e.currentTarget;

				var id = $(target).data('id');

				if ($('#lat-hidden-'+id).length){

					var coordinates = $('#lat-hidden-'+id).val();

					coordinates = coordinates.split(',');

					var latLng = new google.maps.LatLng(coordinates[0], coordinates[1]);

					this.triangleCoords.splice($.inArray(latLng, this.triangleCoords), 1);

					$('#lat-hidden-'+id).remove();

				}

				$('.vertice-'+id).remove();

				this.vertice = this.vertice - 1;

				if( $('#zone-map-canvas-edit').length ){
		        	this.initEdit();
		        }


				for (var i = 0; i < this.markers.length; i++) {
		            if (this.markers[i].id == id) {
		                //Remove the marker from Map                  
		                this.markers[i].setMap(null);
		 
		                //Remove the marker from array.
		                this.markers.splice(i, 1);
		                return;
		            }
		        }



				
			}

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});
	
	$(document).ready(function($) {
		window.MapView = new MapView();
	});	

})( jQuery, this, this.document, this.Misc, undefined );
