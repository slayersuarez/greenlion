/**
* Anuncio Model
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	MapModel = Backbone.Model.extend({
	        defaults: {
	        }

	    ,   initialize: function(){
	            
	        }


	     	/**
	        * Instance controller to send mail
	        *
	        */
	    ,	getZona: function( callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_zonas'
					,	task: 'zonas.getZonas'

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

    });

})( jQuery, this, this.document, this.Misc, undefined );