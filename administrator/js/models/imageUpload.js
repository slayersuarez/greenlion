/**
* Sample Model
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, Utilities ){


	// Extends my object from Backbone model
	imageUploadModel = Backbone.Model.extend({
	        defaults: {
		            id: 0
	        }  

	    ,   initialize: function(){

	    		_.bindAll( 
	    			this, 
	    			'save' );
	            
	        }

	    ,	self: this  

	    	/**
	    	* Saves the event
	    	*
	    	* @param { function } the callback function
	    	* @param { function } the callback error function
	    	*
	    	*/
	    ,	save: function( callback, errorCallback ){

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				} 
				,  aData = {
						option: 'com_anuncios'
					,	task: 'adWizard.uploader'
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return Utilities.ajaxHandler( aOptions, aData );
	    	}

	    	/**
	    	* Saves the event
	    	*
	    	* @param { function } the callback function
	    	* @param { function } the callback error function
	    	*
	    	*/
	    ,	delete: function( data, callback, errorCallback ){

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				} 
				,  aData = {
						option: 'com_anuncios'
					,	task: 'anuncios.deleteImage'
					,	data: data
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return Utilities.ajaxHandler( aOptions, aData );
	    	}	
    });

})( jQuery, this, this.document, this.Misc, undefined );