/**
* Image Uploader View
* version : 1.0
* package: casainmobiliaria.frontend
* package: casainmobiliaria.frontend.mvc
* author: 
* Creation date: August 2014
*
* Allows to upload images for ads in Virtuemart
*
*/
( function( $, window, document, Utilities ){	

	// Create a var to manage the events
	var imageUploadView = {};

	// Extends my object from Backbone events
	imageUploadView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'click .delete-link': 'onRemove'
			}		

		,	_self: this

		,	uploader: {}

		,	imageQue: []	

		,	initialize: function(){

				_.bindAll(
					this,
					'onRemove',
					'onComplete',
					'onCompleteRemove'
				);

				this.imageParams = {

						element: document.getElementById('image-uploader')
					,	uploadButtonText: 'Cargar imágenes'	
					,   action: '../index.php'
					,   params: {

					    	option: 'com_anuncios',
					    	task: 'anuncios.uploadImages'
					    }

					,   allowedExtensions: [ 'jpg', 'JPG', 'png', 'jpeg' ]
					,   debug: false
					,	onSubmit: this.onSubmit
					,	onComplete: this.onComplete
					,   onError: this.onError
				    ,	sizeLimit: 128000000000
				}
				if( $('#image-uploader').length ){
					this.uploader = new qq.FileUploader( this.imageParams );
				}

				this.imageUploadModel = new imageUploadModel();

				this.loadImages();
				this.loadCategorias();
			}

			/**
			*
			*/
		,	onSubmit: function( id, fileName ){

			}

			/**
			*
			*/
		,	onProgress: function( id, fileName, loaded, total ){

				var perc = Math.floor( loaded / total * 100 );
			}

			/**
			*
			*/
		,	onComplete: function(id, fileName, responseJSON){


				if( ! responseJSON.success ){
					$( '.qq-upload-list' ).remove();
					return;
				}

				var li = $( '<li>' )
				,	img = $( '<img>' )
				, 	linkDel = $( '<a>' )
				,	inputHid = $( '<input>' );

				//input hidden
				inputHid.attr({
					'type': 'hidden',
					'name': 'images_up[]'					
				});
				inputHid.val( responseJSON.filename );

				//image element
				img.attr( 'src',  url +'imganuncios/tmp/'+ responseJSON.filename );

				//link element
				linkDel.addClass('delete-link')
					   .text('Eliminar')
					   .attr( {
							'href': '#',
							'data-img': responseJSON.filename 
						});	
				
				li.append(img);
				li.append(linkDel);
				li.append(inputHid);
				$( '.content-images > ul' ).append(li);

				this.imageQue.push( responseJSON.filename );		

				$( '.qq-upload-list' ).remove();
			}

		,	loadImages: function(){

				if ($('#images').length > 0) {

					var images = $('#images').val();

					images = $.parseJSON( images );	

					$.each(images, function(index, val) {

							var li = $( '<li>' )
						,	img = $( '<img>' )
						, 	linkDel = $( '<a>' ),	inputHid = $( '<input>' );

						//input hidden
						inputHid.attr({
							'type': 'hidden',
							'name': 'images_up[]'					
						});
						inputHid.val( val );

						//image element
						img.attr( 'src',  url +'images/stories/virtuemart/product/'+ val );

						//link element
						linkDel.addClass('delete-link')
							   .text('Eliminar')
							   .attr( {
									'href': '#',
									'data-img': val
								});	
						
						li.append(img);
						li.append(inputHid);
						li.append(linkDel);

						$( '.content-images > ul' ).append(li);	
						
					});

				}

			}

		,	loadCategorias: function(){

				if ($('#categories').length > 0) {

					var categorias = $('#categories').val();

					categorias = $.parseJSON( categorias );

					var subcategorias = [];

					$.each(categorias, function(index, val) {

						subcategorias.push( val );
						
					});

					categorias = categorias.toString();

					$('#subcategorias').val( categorias );

					$('#subcategorias').select2({ tags: subcategorias });
				}
			}

			/**
			*
			*/
		,	onRemove: function( e ){

				e.preventDefault();

				var target = e.currentTarget;
				var parentLi = $( target ).parent('li');
	
				var _data = {
						'filename': $( target ).data( 'img' )
					,	'imgindex': $( ".content-images > ul > li" ).index( parentLi )
				};				

				this.imageUploadModel.delete( _data, this.onCompleteRemove, this.onError );
			}

			/**
			*
			*/
		,	onCompleteRemove: function( data ){


				if( data.success ){

					var index = _.indexOf(this.imageQue, data.filename );

					this.imageQue.splice( index, 1 ); 

					$( ".content-images > ul > li" ).filter(':eq('+ data.imgindex +')')
													.animate({opacity:'0.1'}, 
														'fast', 
														function() {$( this ).remove();});
					return;
				}


				return Utilities.showNotification( 'error', 'No se pudo eliminar la imagen'+ data.filename , 0 );				
			}

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				Utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}


	});	

	$(document).ready(function($) {
		window.imageUploadView = new imageUploadView();

    	$("#e1").select2(); 
    	$("#e2").select2();
    	$("#e3").select2();
    	$("#categorias").select2();
    	$("#descuentos").select2();
    	$("#search_categorias").select2();
    	$("#ciudad").select2();

    	$.each($("select[name='plan']"), function(index, val) {

    		$( this ).select2();
    	});
 
	});		

})( jQuery, this, this.document, this.Misc, 'undefined' );