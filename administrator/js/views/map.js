/**
* Google Maps View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var MapView = {};

	// Extends my object from Backbone events
	MapView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'keyup #direccion': 'renderAddressOnMap',
				'click .button-vertice': 'addVertice',
				'click .pac-input': 'initProductMap',
				'click .add-zone': 'renderZone',
				'click .delete-item': 'deleteVertice'

			}

		,	view: this

		,	mapOptions: {
				center: new google.maps.LatLng( 4.626730702566083,-74.080810546875  ),
				zoom: 10,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}



		,	vertice : 0

		,	uniqueId : 1

		,	map: {}

		,	geocoder: {}

		,	initialize: function(){

				_.bindAll(
					this, 
					'initMap',
					'initProductMap',
					'addListeners',
					'onClickMap',
					'renderAddressOnMap',
					'placeMarker',
					'renderZone',
					'deleteVertice',
					'initEditMap',
					'initEdit',
					'addVertice'
				);

				this.mapa = new MapModel();

				if( $('#map_canvas').length ){
					this.initMap();
					this.addListeners();
				}

				
				if( $('#zone-map-canvas').length ){
					this.mapZone = new google.maps.Map(document.getElementById('zone-map-canvas'), this.mapOptions);
				}	
					
				google.maps.event.addDomListener(window, 'load', this.initProductMap());

				this.triangleCoords = [];
				this.markers = [];

				this.bermudaTriangle;



				if( $('#zone-map-canvas-edit').length ){
					this.mapZone = new google.maps.Map(document.getElementById('zone-map-canvas-edit'), this.mapOptions);
					this.initEditMap();
					// this.addListeners();
					
				}
				
			}

		,	initEdit: function(){

				var _this = this;


				if (this.triangleCoords.length < 3)
					utilities.showNotification('error', 'Debe añadir minímo tres puntos en el mapa para pintar la zona.',2000);

				var center = $('.hidden').val().split(',');

				this.mapOptions = {
					center: new google.maps.LatLng( center[1], center[0] ),
					zoom: 13,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};


               	if (_this.vertice < 3)
					$('.add-zone').css({'display':'none'});


				this.mapZone = new google.maps.Map(document.getElementById('zone-map-canvas-edit'), this.mapOptions);

				var triangleCoords = [];

				$.each(this.triangleCoords, function(index, val) {


					triangleCoords.push(new google.maps.LatLng(val.D, val.k));

					var latLng = new google.maps.LatLng(val.D, val.k);

					var marker = new google.maps.Marker({
						map: _this.mapZone,
						position: latLng
					});

					marker.id = _this.uniqueId;
		            _this.uniqueId++;

		            _this.markers.push(marker);
				});

				if ( this.bermudaTriangle != undefined ) {
					this.bermudaTriangle.setMap(null);
				}


				// Construct the polygon.
				this.bermudaTriangle = new google.maps.Polygon({
					paths: triangleCoords,
					strokeColor: $('#color').val(),
					strokeOpacity: 0.8,
					strokeWeight: 3,
					fillColor: $('#color').val(),
					fillOpacity: 0.35
				});
				

				this.bermudaTriangle.setMap(this.mapZone);

				$('#zone').val('1');


			}


		,	initEditMap: function(){


					var _this = this;

					var center = $('.hidden').val().split(',');

					this.mapOptions = {
						center: new google.maps.LatLng( center[1], center[0] ),
						zoom: 13,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};

					this.mapZone = new google.maps.Map(document.getElementById('zone-map-canvas-edit'), this.mapOptions);


					var triangleCoords = [];


					$('.hidden').each(function(){

					    var coordinates = $(this).val().split(',');

					    var latLng = new google.maps.LatLng(coordinates[1], coordinates[0]);

					    var marker = new google.maps.Marker({
							map: _this.mapZone,
							position: latLng
						});

						marker.id = _this.uniqueId;
			            _this.uniqueId++;

			            _this.markers.push(marker);

						triangleCoords.push(latLng);

						_this.triangleCoords.push(new google.maps.LatLng(coordinates[0], coordinates[1]));

						var address = coordinates[1]+','+coordinates[0];

						var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + address + "&sensor=false";

			            jQuery.getJSON(url, function (json) {

			            	_this.vertice = _this.vertice + 1;

			                var formattedAddress = json.results[0].formatted_address;

							var html = '<li class="vertice-'+_this.vertice+'"><input class="pac-input" value="'+formattedAddress+'" id="pac-input'+_this.vertice+'" class="controls" type="text"><a href="#" class="delete-item" data-id="'+_this.vertice+'"><img src="../images/icono-basura.png"></a></li>';

			               	$('.vertices ul').append(html);

			               	if (_this.vertice >= 3)
								$('.add-zone').css({'display':'inline-block'});
			            });

					});

					// Construct the polygon.
					this.bermudaTriangle = new google.maps.Polygon({
						paths: triangleCoords,
						strokeColor: $('#color').val(),
						strokeOpacity: 0.8,
						strokeWeight: 3,
						fillColor: $('#color').val(),
						fillOpacity: 0.35
					});

					this.bermudaTriangle.setMap(this.mapZone);

					$('#zone').val('1');
				
			}

		,	renderZone: function(){



				if (this.triangleCoords.length < 3)
					return utilities.showNotification('error', 'Debe añadir minímo tres puntos en el mapa para pintar la zona.',2000);

				// var map = new google.maps.Map(
				// 	document.getElementById("zone-map-canvas"),
				// 	this.mapOptions
				// );


				var triangleCoords = [
				];

				$.each(this.triangleCoords, function(index, val) {
					triangleCoords.push(new google.maps.LatLng(val.D, val.k));
				});

				if ( this.bermudaTriangle != undefined ) {
					this.bermudaTriangle.setMap(null);
				}

				
				// Construct the polygon.
				this.bermudaTriangle = new google.maps.Polygon({
					paths: triangleCoords,
					strokeColor: $('#color').val(),
					strokeOpacity: 0.8,
					strokeWeight: 3,
					fillColor: $('#color').val(),
					fillOpacity: 0.35
				});
	

				this.bermudaTriangle.setMap(this.mapZone);

				$('#zone').val('1');

			}

			/**
			* Initializes the map on the canvas
			*
			*/
		,	initMap: function(){

				var _self = this;

				if( $( '#latlang' ).length ){

					var latlng = $( '#latlang' ).val();

					if( latlng != '' ){

						latlng = latlng.split(',', 2 );

						var lat = parseFloat(latlng[0]);
						var lng = parseFloat(latlng[1]);

						var latlng = new google.maps.LatLng(lat, lng);

						this.mapOptions.center = latlng;

					}
				}

				this.map = new google.maps.Map(
					document.getElementById("map_canvas"),
					this.mapOptions
				);

				this.marker = new google.maps.Marker({
					map: this.map,
					draggable: false
				});

				this.geocoder = new google.maps.Geocoder();

				this.mapa.getZona( function( data ){

					$.each(data.vertices, function(index, vertice) {

						var triangleCoords = [];

						$.each(vertice.vertices, function(index, ver) {
							
							triangleCoords.push(new google.maps.LatLng(ver.longitud, ver.latitud));

						});

						_self.bermudaTriangle = new google.maps.Polygon({
							paths: triangleCoords,
							strokeColor: vertice.color,
							strokeOpacity: 0.8,
							strokeWeight: 3,
							fillColor: vertice.color,
							fillOpacity: 0.35,
							draggable: true,
							geodesic: true
						});

						_self.bermudaTriangle.setMap(_self.map);

					});

				}, this.onError);

			

				if( typeof latlng == 'object' ){

					_self.geocoder.geocode( { 'latLng': latlng }, function( results, status ) {

						if ( status == google.maps.GeocoderStatus.OK ) {
							if ( results[1] ) {
								_self.marker = new google.maps.Marker({
									position: latlng,
									map: _self.map
								});

								// infowindow.setContent(results[1].formatted_address);
								// infowindow.open(map, marker);
							} else {
								utilities.showNotification( 'warning', 'No se pudo recuperar la localización del anuncio en el mapa.', 3000);
							}
						} else {
							utilities.showNotification( 'error', 'La geocodificación de google maps debido a: ' + status, 3000 );
						}
					});
				}


			}

			/**
			* Initializes the product map
			*
			*/
		,	initProductMap: function( e ){

				this.marker = {};
				
				if (e != undefined) {

					var _this = this;

					if (e == 'algo') {

						var dir = 'Bogotá, Colombia';

						$( '#pac-input'+this.vertice ).val( dir );


						this.marker = new google.maps.Marker({ 
					       	map: this.mapZone,
					       	position: new google.maps.LatLng( 4.598056, -74.07583299999999 ),
					       	draggable: true
				    	});

					}


					google.maps.event.addListener(this.marker, 'dragstart', function() {

			            var a = _this.marker.getPosition();

			            console.log( a );

			            if ( _this.triangleCoords[_this.triangleCoords.length - 1].k == a.D) {

			            	var latLng = new google.maps.LatLng(_this.triangleCoords[_this.triangleCoords.length - 1].k, _this.triangleCoords[_this.triangleCoords.length - 1].D);

							_this.triangleCoords.splice($.inArray(latLng, _this.triangleCoords), 1);
			            }
					    
					});
  

					google.maps.event.addListener(this.marker, 'drag', function() {

						_this.marker.id = _this.uniqueId;
			            _this.uniqueId++;

			            _this.markers.push(_this.marker);

			            var a = _this.marker.getPosition();
			            
					   	_this.getAddress(a.k + ',' + a.D);

						
					});

					google.maps.event.addListener(this.marker, 'dragend', function() {

						_this.marker.id = _this.uniqueId;
			            _this.uniqueId++;

			            _this.markers.push(_this.marker);

			            var a = _this.marker.getPosition();

						_this.triangleCoords.push(new google.maps.LatLng(a.D, a.k));


					});	


					
					var markers = [];

					var mapOptions = {
						center: new google.maps.LatLng( 4.626730702566083,-74.080810546875  ),
						zoom: 11,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};

					var input = (document.getElementById('pac-input'+this.vertice));

					var searchBox = new google.maps.places.SearchBox((input));

					google.maps.event.addListener( this.mapZone, 'click', function(e) {

						_this.marker.setMap(null);

					   	_this.marker = new google.maps.Marker({ 
					       	map: _this.mapZone,
					       	position: e.latLng,
					       	draggable: true
					    });

					   	//Set unique id
			            _this.marker.id = _this.uniqueId;
			            _this.uniqueId++;

			            _this.markers.push(_this.marker);

					   	_this.getAddress(e.latLng.k + ',' + e.latLng.D);
						_this.triangleCoords.push(new google.maps.LatLng(e.latLng.D, e.latLng.k));

						var inputlat = '<input type="hidden" name="coordinates[]" id="lat-hidden-'+_this.vertice+'" value="'+e.latLng.D+','+e.latLng.k+'"/>';

						$('.add-vertice').append(inputlat);


						google.maps.event.addListener(_this.marker, 'dragstart', function() {

				            var a = _this.marker.getPosition();

				            if ( _this.triangleCoords[_this.triangleCoords.length - 1].k == a.D) {

				            	var latLng = new google.maps.LatLng(_this.triangleCoords[_this.triangleCoords.length - 1].k, _this.triangleCoords[_this.triangleCoords.length - 1].D);

								_this.triangleCoords.splice($.inArray(latLng, _this.triangleCoords), 1);
				            }
						    
						});
	  

						google.maps.event.addListener(_this.marker, 'drag', function() {

							_this.marker.id = _this.uniqueId;
				            _this.uniqueId++;

				            _this.markers.push(_this.marker);

				            var a = _this.marker.getPosition();
				            
						   	_this.getAddress(a.k + ',' + a.D);

							
						});

						google.maps.event.addListener(_this.marker, 'dragend', function() {

							_this.marker.id = _this.uniqueId;
				            _this.uniqueId++;

				            _this.markers.push(_this.marker);

				            var a = _this.marker.getPosition();

							_this.triangleCoords.push(new google.maps.LatLng(a.D, a.k));


						});	


					});


					// [START region_getplaces]
					// Listen for the event fired when the user selects an item from the
					// pick list. Retrieve the matching places for that item.
					google.maps.event.addListener(searchBox, 'places_changed', function() {

						_this.places = searchBox.getPlaces();
						
						for (var i = 0, place; place = places[i]; i++) {

							//_this.marker.setMap(null);

							var image = {
								url: place.icon,
								size: new google.maps.Size(71, 71),
								origin: new google.maps.Point(0, 0),
								anchor: new google.maps.Point(17, 34),
								scaledSize: new google.maps.Size(25, 25)
							};

							//Create a marker for each place.
							_this.marker = new google.maps.Marker({
								map: _this.mapZone,
								title: _this.place.name,
								position: _this.place.geometry.location,
								draggable: true
							});

							//Set unique id
				            _this.marker.id = _this.uniqueId;
				            _this.uniqueId++;

				            _this.markers.push(_this.marker);

							_this.triangleCoords.push(new google.maps.LatLng(place.geometry.location.D, place.geometry.location.k));

							var inputlat = '<input type="hidden" name="coordinates[]" id="lat-hidden-'+_this.vertice+'" value="'+place.geometry.location.D+','+place.geometry.location.k+'"/>';

							$('.add-vertice').append(inputlat);


							google.maps.event.addListener(_this.marker, 'dragstart', function() {

					            var a = _this.marker.getPosition();

					            if ( _this.triangleCoords[_this.triangleCoords.length - 1].k == a.D) {

					            	var latLng = new google.maps.LatLng(_this.triangleCoords[_this.triangleCoords.length - 1].k, _this.triangleCoords[_this.triangleCoords.length - 1].D);

									_this.triangleCoords.splice($.inArray(latLng, _this.triangleCoords), 1);
					            }
							    
							});
		  

							google.maps.event.addListener(_this.marker, 'drag', function() {

								_this.marker.id = _this.uniqueId;
					            _this.uniqueId++;

					            _this.markers.push(_this.marker);

					            var a = _this.marker.getPosition();
					            
							   	_this.getAddress(a.k + ',' + a.D);

								
							});

							google.maps.event.addListener(_this.marker, 'dragend', function() {

								_this.marker.id = _this.uniqueId;
					            _this.uniqueId++;

					            _this.markers.push(_this.marker);

					            var a = _this.marker.getPosition();

								_this.triangleCoords.push(new google.maps.LatLng(a.D, a.k));


							});


						}

					});
					

				}

			}
			/**
			* Add the map listeners to the event
			*
			*/
		,	addListeners: function(){

				google.maps.event.addListener( this.map, 'click', this.onClickMap );

				return;
			}


			/**
			* Catch the map address when user clicks on the map
			*
			*/
		,	onClickMap: function( e ){

				this.placeMarker( e.latLng );
				this.getAddress( e.latLng.k + ',' + e.latLng.D );
			}

			/**
			* Render the address inserted by user on the map
			*
			*/
		,	renderAddressOnMap: function( e ){

				var target = e.currentTarget;

				var address = $( target ).val();

				var _self = this;

				this.geocoder.geocode( { 'address': address}, function(results, status) {

					if (status == google.maps.GeocoderStatus.OK) {

						_self.map.setCenter(results[0].geometry.location);
						_self.marker.setPosition( results[0].geometry.location );

					} else {

						return;
					}
				});
			}

			/**
			* Get the read human address from latitud and longitude
			*
			*/
		,	getAddress: function( latLng ){


				var _this = this;

				var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latLng + "&sensor=false";

				//$( '#latlang' ).val( latLng );

	            jQuery.getJSON(url, function (json) {

	                
	                var formattedAddress = json.results[0].formatted_address;
	                // var departamento = json.results[0].address_components[4].long_name;
	                // var city = json.results[0].address_components[3].long_name;
	                // var neighborhood = json.results[1].address_components[0].long_name;
	                
	                formattedAddress = formattedAddress.replace( new RegExp("[,\s]+[ña-zA-Z _]+[,\s]+[ña-zA-Z _]+[ña-zA-Z _]+"), '' );

	                $( '#pac-input'+_this.vertice ).val( formattedAddress );
	            }); 

			}

			/**
			* Place the marker into the map when user clicks on
			*
			*/
		,	placeMarker: function ( location ){

				this.marker.setPosition( location );
				return;
			}

		,	addVertice: function(e){

				e.preventDefault();

				this.vertice = this.vertice + 1;

				var html = '<li class="vertice-'+this.vertice+'"><input class="pac-input" id="pac-input'+this.vertice+'" class="controls" type="text"><a href="#" class="delete-item" data-id="'+this.vertice+'"><img src="../images/icono-basura.png"></a></li>';

				if (this.vertice >= 3)
					$('.add-zone').css({'display':'inline-block'});

				if (this.vertice > 4) {
					alert('No puede añadir más vertices');
				}else{
					$('.vertices ul').append(html);
					this.initProductMap( 'algo' );
				}

			}

		,	deleteVertice: function(e) {

				e.preventDefault();

				var target = e.currentTarget;

				var id = $(target).data('id');

				if ($('#lat-hidden-'+id).length){

					var coordinates = $('#lat-hidden-'+id).val();

					coordinates = coordinates.split(',');

					var latLng = new google.maps.LatLng(coordinates[0], coordinates[1]);

					this.triangleCoords.splice($.inArray(latLng, this.triangleCoords), 1);

					$('#lat-hidden-'+id).remove();

				}

				$('.vertice-'+id).remove();

				this.vertice = this.vertice - 1;

				if( $('#zone-map-canvas-edit').length ){
		        	this.initEdit();
		        }


				for (var i = 0; i < this.markers.length; i++) {
		            if (this.markers[i].id == id) {
		                //Remove the marker from Map                  
		                this.markers[i].setMap(null);
		 
		                //Remove the marker from array.
		                this.markers.splice(i, 1);
		                return;
		            }
		        }



				
			}

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});
	
	$(document).ready(function($) {
		window.MapView = new MapView();
	});	

})( jQuery, this, this.document, this.Misc, undefined );