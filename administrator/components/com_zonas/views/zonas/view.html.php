<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class ZonasViewZonas extends JViewLegacy {

	protected $zonas;
	protected $zona;
	protected $state;
	protected $pagination;
	protected $vertices;

	
	// Function that initializes the view
	function display( $tpl = null ){

		$this->zonas = $this->get('Objects');
		$this->state = $this->get('State');
		$this->pagination = $this->get('Pagination');
	
		// Add the toolbar with the actions
		$this->addToolbar();
	
		parent::display( $tpl );
	
	}
	
	// Show the toolbar with CRUD modules
	protected function addToolbar(){
	
		JToolBarHelper::title( "Gestión de Zonas", 'cpanel' );

		$layout = $this->getLayout();

		if ($layout == 'new') {
			JToolBarHelper::cancel( "zonas.cancel" );
			JToolBarHelper::divider();
			JToolBarHelper::save("zonas.save");

		}elseif($layout == 'edit'){
			JToolBarHelper::cancel( "zonas.cancel" );
			JToolBarHelper::divider();
			JToolBarHelper::save("zonas.editZone");	
		}
		else{
			JToolBarHelper::addNew( "zonas.nuevo" );
			JToolBarHelper::divider();
			JToolBarHelper::editList( "zonas.edit" );
			JToolBarHelper::divider();
			JToolBarHelper::trash( "zonas.delete" );
			JToolBarHelper::divider();
			JToolBarHelper::publish( "zonas.publish" );
			JToolBarHelper::divider();
			JToolBarHelper::unpublish( "zonas.publish" );


		}
	}

}
?>