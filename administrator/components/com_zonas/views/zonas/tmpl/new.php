<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
JHtml::_('behavior.formvalidation');

//add the links to the external files into the head of the webpage (note the 'administrator' in the path, which is not nescessary if you are in the frontend)
$document =& JFactory::getDocument();
$document->addStyleSheet('components/com_zonas/assets/css/style.css');
$document->addStyleSheet('components/com_zonas/assets/css/colorpicker.css');
$document->addStyleSheet('components/com_zonas/assets/css/layout.css');


$document->addScript( '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
$document->addScript( 'components/com_zonas/assets/js/colorpicker.js');
$document->addScript( 'components/com_zonas/assets/js/eye.js');
$document->addScript( 'components/com_zonas/assets/js/utils.js');
$document->addScript( 'components/com_zonas/assets/js/layout.js?ver=1.0.2');

?>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'zonas.cancel' || document.formvalidator.isValid(document.id('zona-form'))) {
			Joomla.submitform(task, document.getElementById('zona-form'));
		}
	}
</script>




<form name="adminForm" id="zona-form" method="post" action="<?php echo JRoute::_('');?>" class="form-validate">
	<fieldset class="adminform">
		<legend>Gestionar Zona</legend>
		<div class="form-zona">
			<ul>
				<li><label for="titulo">Titulo de Zona: </label><input type="text" name="titulo" class="inputbox required" value="<?php //echo $this->plan->titulo ?>" aria-required="true" required="required" aria-invalid="true"/></li>
				<li>
					<label for="color">Color: </label>
					<input type="text" id="color" name="color" value="#FFF" readonly>

					<div id="colorSelector"><div style="background-color: #FFF"></div></div>

				</li>

				<li>	
					<label>Estado: </label>
					<fieldset id="jform_block" class="radio">
						<input type="radio" id="jform_block0" name="jform[block]" value="0" checked="checked">
						<label for="jform_block0">Desactivado</label>
						<input type="radio" id="jform_block1" name="jform[block]" value="1">
						<label for="jform_block1">Activado</label>
					</fieldset>
				</li>
			</ul>

			<br>
			<hr>
			<br>
			
			<div id="zone-map-canvas"></div>

			<div style="width: 100%;">
				<div class="add-vertice">
					<ul>
						<li><a class="button-vertice" href="#">+ VERTICE</a></li>
					</ul>

				</div>

				<div class="vertices">
					<ul>
				
					</ul>

				</div>
			</div>


			<input type="hidden" name="id" value="<?php //echo $this->plan->id ?>" />

			<input type="hidden" id="zone" name="zone" value="0" />

			<div class="add-zone-wrapper">
				<a href="#" class="add-zone" style="display: none;">Añadir zona</a>
			</div>
			
		</div>

	</fieldset>

	<input type="hidden" name="task" value="" />
	<input type="hidden" name="option" value="com_zonas" />
</form>