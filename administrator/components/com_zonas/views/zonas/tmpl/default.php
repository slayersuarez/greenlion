<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();

//add the links to the external files into the head of the webpage (note the 'administrator' in the path, which is not nescessary if you are in the frontend)
$document =& JFactory::getDocument();
$document->addScript( '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
// $document->addScript( $host.'administrator/components/com_certificados/assets/js/libs/fileuploader/fileuploader.js');
// $document->addStyleSheet($host.'administrator/components/com_certificados/assets/js/libs/fileuploader/fileuploader.css');
$document->addStyleSheet($host.'administrator/components/com_zonas/assets/css/style.css');
// $document->addScript( $host.'administrator/components/com_certificados/assets/js/misc/misc.js');
// $document->addScript( $host.'administrator/components/com_certificados/assets/js/views/tecnico.js');
// $document->addScript( $host.'administrator/components/com_certificados/assets/js/models/tecnico.js');
// $document->addScript( $host.'administrator/components/com_certificados/assets/js/controllers/tecnico.js');
// $document->addScript( $host.'administrator/components/com_certificados/assets/js/handlers/tecnico.js');
// $document->addScript( $host.'administrator/components/com_certificados/assets/js/misc/file.js');
?>



<form action="<?php echo JRoute::_('');?>" method="post" name="adminForm" id="adminForm">
	<div class="clr"> </div>

	<table class="adminlist">
		<thead>
			<tr>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th class="center" width="10%">
					Id
				</th>
				<th class="nowrap" width="10%">
					Titulo de Zona
				</th>
				<th class="nowrap" width="20%">
					Estado
				</th>
				<th class="nowrap" width="5%">
					Color
				</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($this->zonas as $i => $item) :
		?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center">
					<?php echo JHtml::_('grid.id', $i, $item->id); ?>
				</td>
				<td class="center">
					<?= $item->id ?>
				</td>
				<td class="center">
					<a href="index.php?option=com_zonas&task=zonas.edit&cid=<?= $item->id ?>"><?= $item->titulo ?></a>
				</td>
				<td class="center">
					<?php echo JHtml::_('grid.boolean', $i, $item->estado, 'zonas.publish', 'zonas.publish'); ?>
				</td>
				<td class="center">
					<span class="color" style="background-color: <?= $item->color ?>"></span>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

<hr>
<br>

<div id="map_canvas">
	

</div>