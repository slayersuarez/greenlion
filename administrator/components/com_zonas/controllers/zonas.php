
<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

// Begining of the controller
class ZonasControllerZonas extends JControllerLegacy{

	public function nuevo(){


		$view = $this->getView( 'zonas', 'html' );

		$view->setLayout( 'new' );
		$view->display();
	}

	public function save(){

		$app = JFactory::getApplication();
		$input = $app->input;

		// Get the post from data
		$data = $input->getArray( $_POST );

		

		if (! isset($data['coordinates'])){
			$this->setRedirect('index.php?option=com_zonas&task=zonas.nuevo','Debe añadir las vertices al mapa.','warning');
			return;
		}

		if ( count( $data['coordinates'] ) < 3 ){
			$this->setRedirect('index.php?option=com_zonas&task=zonas.nuevo','Debe agregar mínimo tres vertices.','warning');
			return;
		}

		if ($data['zone'] == '0'){
			$this->setRedirect('index.php?option=com_zonas&task=zonas.nuevo','Debe añadir la zona al mapa.','warning');
			return;
		}

		
		// save zona
		$zonaModel = $this->getModel( 'zonas' );

		$args = array(
				'titulo' => $data['titulo']
			,	'color' => $data['color']
			,	'estado' => $data['jform']['block']
		);

		$zonaModel->instance( $args );

		if ( ! $zonaModel->save('bool') ) {
			$this->setRedirect('index.php?option=com_zonas&task=zonas.nuevo','No se pudo guardar la zona','warning');
		}

		// last zona insert
		$id = $zonaModel->insertId;

		// save vertices
		

		foreach ($data['coordinates'] as $key => $coordenada) {

			list( $lat, $lng ) = explode(',', $coordenada);

			$verticesModel = $this->getModel( 'vertices' );

			$verticesArgs = array(
					'id_zona' => $id
				,	'latitud' => $lat
				,	'longitud' => $lng
			);

			$verticesModel->instance( $verticesArgs );

			$verticesModel->save();
		}

		$this->setRedirect('index.php?option=com_zonas','Zona guardada correctamente','success');

	
	}

	public function editZone(){

		$app = JFactory::getApplication();
		$input = $app->input;

		// Get the post from data
		$data = $input->getArray( $_POST );

		if (! isset($data['coordinates'])) 
			$this->setRedirect('index.php?option=com_zonas&task=zonas.nuevo','Debe añadir las vertices al mapa.','warning');
		

		if ( count( $data['coordinates'] ) < 3 )
			$this->setRedirect('index.php?option=com_zonas&task=zonas.nuevo','Debe agregar mínimo tres vertices.','warning');

		if ($data['zone'] == '0')
			$this->setRedirect('index.php?option=com_zonas&task=zonas.nuevo','Debe añadir la zona al mapa.','warning');

		
		// save zona
		$zonaModel = $this->getModel( 'zonas' );

		$args = array(
				'id' => $data['id']
			,	'titulo' => $data['titulo']
			,	'color' => $data['color']
			,	'estado' => $data['jform']['block']
		);

		$zonaModel->instance( $args );

		if ( ! $zonaModel->save('bool') ) {
			$this->setRedirect('index.php?option=com_zonas&task=zonas.nuevo','No se pudo guardar la zona','warning');
		}

		// last zona insert
		$id = $data['id'];

		$verticesModel = $this->getModel( 'vertices' );

		$verticesModel->deleteVertices( $id );

		//save vertices
		foreach ($data['coordinates'] as $key => $coordenada) {


			list( $lat, $lng ) = explode(',', $coordenada);

			$verticesModel = $this->getModel( 'vertices' );

			$verticesArgs = array(
					'id_zona' => $id
				,	'latitud' => $lat
				,	'longitud' => $lng
			);

			$verticesModel->instance( $verticesArgs );

			$verticesModel->save();
		}

		$this->setRedirect('index.php?option=com_zonas','Zona guardada correctamente','success');

	
	}

	public function edit(){

		$cid = JRequest::getVar( 'cid' );

		

		$zonaModel = $this->getModel( 'zonas' );

		if (is_array($cid)) {
			$zona = $zonaModel->getObject( $cid[0] );
		}else{
			$zona = $zonaModel->getObject( $cid );
		}

		

		$verticesModel = $this->getModel( 'vertices' );

		$vertices = $verticesModel->getVertices( $zona->id );

		$view = $this->getView( 'zonas', 'html' );

		$view->assignRef( 'zona', $zona );
		$view->assignRef( 'vertices', $vertices );

		$view->setLayout( 'edit' );

		$view->display();

	}

	public function delete(){

		$cid = JRequest::getVar( 'cid' );
		
		$zonaModel = $this->getModel( 'zonas' );
		$verticesModel = $this->getModel( 'vertices' );


		foreach ($cid as $key => $id) {

			$zona = $zonaModel->getObject( $id );

			$zonaModel->instance( $id );
			$zonaModel->delete();

			$args = array('id_zona' => $id );

			$verticesModel->instance( $args );
			$verticesModel->delete();

		}

		$this->setRedirect( 'index.php?option=com_zonas', 'Zona borrada', 'success'  );

	}

	public function publish(){

		$cid = JRequest::getVar('cid');

		foreach ($cid as $key => $id) {
			
			$model = $this->getModel( 'zonas' );
			$model->instance( $id );

			$model->publish();
		}

		$this->setRedirect( 'index.php?option=com_zonas' );
	}

	public function getZonas(){

		$response = new stdClass();

		$zonaModel = $this->getModel( 'zonas' );
		$zonas = $zonaModel->getZonas();


		foreach ($zonas as $key => $zona) {
			
			$verticesModel = $this->getModel( 'vertices' );

			$vertice = $verticesModel->getVertices( $zona->id );

			$zona->vertices = $vertice;
		}


		$response->vertices = $zonas;
		echo json_encode($response);
		die;

	}



}
?>