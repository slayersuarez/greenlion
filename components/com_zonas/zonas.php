<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . DS . 'controller.php' );

$controller = JControllerLegacy::getInstance('zonas');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>