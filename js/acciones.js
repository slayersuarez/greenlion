 /**
* Menu JS
*
*/
( function( $, window, document ){

		$(window).stellar();
		
		var GreenLionView = function( a ){

		};

		GreenLionView.prototype = {

			/**
			* Initialize the view
			*
			*/
			initialize: function(){

				var _this = this;

				this.goToProgramas();
				this.goToHeader();

				$( window ).resize( function(){

					_this.resizeBGImages( '.bgresize' );


				});
			},

			goToProgramas: function(){


				$( '.go-to-programas' ).click( function( e ){

					e.preventDefault();

					$('html, body').animate({
				        scrollTop: $("#section-two").offset().top
				    }, 800);

				});
			},

			goToHeader: function(){


				$( '.go-to-header' ).click( function( e ){

					e.preventDefault();

					$('html, body').animate({
				        scrollTop: $("#section-one").offset().top
				    }, 800);

				});
			},

			/**
			* Resize the background images
			*
			*/
			resizeBGImages: function( selector ){


					// console.log( 'here' );
					// var ancho= window.innerWidth;
					// var margen= (1900-ancho)/2;
					// var x;
					// x=jQuery( selector );
					// x.css("margin-left","-"+margen+"px");
					//$( selector ).css( 'background-position-x' );

			}
		}

		window.GreenLionView = new GreenLionView();
		window.GreenLionView.initialize();

	

})( jQuery, this, this.document, undefined );